#[macro_use] extern crate numrs;

#[cfg(test)] mod numrs_test {
    mod common;

    mod array;
    mod array_masked;

    mod ops;

    mod axis_array;

    mod broadcast_array;

    mod display_array;

    mod hist_array;

    mod ext_primitives;
    mod ext_vec;
}