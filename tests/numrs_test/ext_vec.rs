use numrs::ext::vec::lexsort;
use numrs::prelude::*;

#[test] fn stats_sum_usize_test() { assert_eq!(vec![1, 2, 3].sum(), 6) }
#[test] fn stats_mult_usize_test() { assert_eq!(vec![1, 2, 3].mult(), 6) }

#[test] fn to_mask_neg_test() { assert_eq!(vec![1., 2., -3.].to_mask_neg(), vec![0, 0, 1]) }

#[test] fn filter_by_mask_test() { assert_eq!(vec![(1., 0), (2., 0), (-3., 1)].filter_by_mask(), vec![1., 2.]) }

#[test] fn concatenate_test() {
    let arr = array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]];
    assert_eq!(vec![arr.clone(); 3].concatenate(None), array![1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5.]);
    assert_eq!(vec![arr.clone(); 3].concatenate(Some(0)), array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]]);
    assert_eq!(vec![arr.clone(); 3].concatenate(Some(1)), array![[[1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.]], [[1., 3.], [3., 5.], [1., 3.], [3., 5.], [1., 3.], [3., 5.]]]);
    assert_eq!(vec![arr; 3].concatenate(Some(2)), array![[[1., 2., 1., 2., 1., 2.], [1., 2., 1., 2., 1., 2.]], [[1., 3., 1., 3., 1., 3.], [3., 5., 3., 5., 3., 5.]]]);
    let arr_ma = array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]];
    assert_eq!(vec![arr_ma.clone(); 3].concatenate(None), array![1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5.]);
    assert_eq!(vec![arr_ma.clone(); 3].concatenate(Some(0)), array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]]);
    assert_eq!(vec![arr_ma.clone(); 3].concatenate(Some(1)), array![[[1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.]], [[1., 3.], [3., 5.], [1., 3.], [3., 5.], [1., 3.], [3., 5.]]]);
    assert_eq!(vec![arr_ma; 3].concatenate(Some(2)), array![[[1., 2., 1., 2., 1., 2.], [1., 2., 1., 2., 1., 2.]], [[1., 3., 1., 3., 1., 3.], [3., 5., 3., 5., 3., 5.]]]);
}

#[test] fn lexsort_test() {
    assert_eq!(lexsort(&array![0, 1, 0], &array![1, 2, 0]), array![2, 0, 1]);
    assert_eq!(lexsort(&array![0, 1, 0], &array![0, 2, 1]), array![0, 2, 1]);
    assert_eq!(lexsort(&array![1, 4, 7, 1, 0], &array![12, 2, 1, 12, 2]), array![2, 4, 1, 0, 3]);
    assert_eq!(lexsort(&array![2, 4, 6, 3, 7, 5], &array![8, 3, 6, 4, 5, 2]), array![5, 1, 3, 4, 2, 0]);
    assert_eq!(lexsort(&array![9, 3, 1, 7, 4, 3, 6], &array![4, 6, 9, 2, 1, 8, 7]), array![4, 3, 0, 1, 6, 5, 2]);
    assert_eq!(lexsort(&array![1, 5, 1, 4, 3, 4, 4], &array![9, 4, 0, 4, 0, 2, 1]), array![2, 4, 6, 5, 3, 1, 0]);
}
