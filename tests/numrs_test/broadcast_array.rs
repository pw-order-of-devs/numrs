use rstest::rstest;

use numrs::prelude::*;

use crate::numrs_test::common::test_runner;

#[rstest(
arr, shape, expected,
case(array![1., 2., 3.], vec![2, 3], array![[1., 2., 3.], [1., 2., 3.]]),
case(array![[1., 2., 3.]], vec![2, 3], array![[1., 2., 3.], [1., 2., 3.]]),
case(array![[1., 2., 3.]], vec![3, 3], array![[1., 2., 3.], [1., 2., 3.], [1., 2., 3.]]),
#[should_panic] case(array![1., 2., 3.], vec![3, 2], array![1.]),
#[should_panic] case(array![1., 2., 3.], vec![4, 1], array![1.]),
)] fn broadcast_to_test(arr: Array<f64>, shape: Vec<usize>, expected: Array<f64>) {
    test_runner(&arr.broadcast_to(shape), &expected);
}

#[rstest(
arr, arr_b, expected,
case(array![1., 2., 3.], array![[1., 2., 3.], [1., 2., 3.]], array![[1., 2., 3.], [1., 2., 3.]]),
case(array![1.], array![[1., 2., 3.], [1., 2., 3.]], array![[1., 1., 1.], [1., 1., 1.]]),
case(array![1., 1.], array![[[1., 2.], [3., 1.]], [[2., 3.], [4., 5.]]], array![[[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]]]),
case(array![[[1.], [2.]]], array![[[1., 2., 3.], [4., 5., 6.]]], array![[[1., 1., 1.], [2., 2., 2.]]]),
)] fn broadcast_test(arr: Array<f64>, arr_b: Array<f64>, expected: Array<f64>) {
    test_runner(&Array::<f64>::broadcast(&arr, &arr_b).0, &expected);
}
