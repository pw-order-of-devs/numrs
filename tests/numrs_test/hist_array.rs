use rstest::rstest;

use numrs::prelude::*;
use numrs::base::hist::HistBins;

#[rstest(
arr, bins, expected,
case(
    array_1d((0..100).map(|i| i.to_float()).collect()),
    None,
    (
        array![10., 10., 10., 10., 10., 10., 10., 10., 10., 10.],
        array![0., 9.9, 19.8, 29.7, 39.6, 49.5, 59.4, 69.3, 79.2, 89.1, 99.],
    ),
),
case(
    array![1.,2.,1.],
    Some(HistBins::multi(vec![0, 1, 2, 3])),
    (array![0., 2., 1.], array![0., 1., 2., 3.],),
),
case(
    array![0.,1.,2.,3.],
    Some(HistBins::multi(vec![0, 1, 2, 3, 4])),
    (array![1., 1., 1., 1.], array![0., 1., 2., 3., 4.],),
),
)] fn hist_test(arr: Array<f64>, bins: Option<HistBins>, expected: (Array<f64>, Array<f64>)) {
    assert_eq!(arr.histogram(bins, None, None, None), expected);
}
