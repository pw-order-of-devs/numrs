use rstest::rstest;

use numrs::prelude::*;

use crate::numrs_test::common::{test_runner, test_runner_gen};

#[rstest(
eq, expected,
case(array![1., 2., 3.] + 2, array![3., 4., 5.]),
case(array![1., 2., 3.] + 2., array![3., 4., 5.]),
case(2 + array![1., 2., 3.], array![3, 4, 5]),
case(2. + array![1., 2., 3.], array![3., 4., 5.]),
case(array![1., 2., 3.] + array![4., 5., 6.], array![5., 7., 9.]),
case(array![1., 2., 3.] + array![4., 5., 6.], array![5., 7., 9.]),
case(array![1., 2., 3.] + array![4., 5., 6.], array![5., 7., 9.]),
case(array![1., 2., 3.] + array![4., 5., 6.], array![5., 7., 9.]),
case(array![[1., 2.], [3., 4.]] + array![[5., 6.], [7., 8.]], array![[6., 8.], [10., 12.]]),
case(array![[1., 2.], [3., 4.]] + array![[5., 6.], [7., 8.]], array![[6., 8.], [10., 12.]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] + array![1., 2., 3., 4.], array![0.]),
case(array![[1., 2., 3.], [1., 2., 3.]] + array![1., 2., 3.], array![[2., 4., 6.], [2., 4., 6.]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] + array![1., 2.], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] + array![[1., 2.]], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] + array![[1., 2.], [1., 2.]], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] + array![2., 3., 4.], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] + array![[2., 3., 2., 4.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] + array![[2., 3., 2.], [4., 3., 2.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] + array![[[2., 3., 2.], [4., 3., 2.]]], array![0.]),
)] fn add_test<S1: ArrayType, S2: ArrayType>(
    eq: impl ArrayImpl<S1>,
    expected: impl ArrayImpl<S2>
) {
    test_runner_gen(&eq, &expected)
}

#[rstest(
eq, expected,
case(array![1., 2., 3.] - 2, array![-1., 0., 1.]),
case(array![1., 2., 3.] - 2., array![-1., 0., 1.]),
case(2. - array![1., 2., 3.], array![1., 0., -1.]),
case(array![1., 2., 3.] - array![4., 5., 6.], array![-3., -3., -3.]),
case(array![1., 2., 3.] - array![4., 5., 6.], array_ma![(-3., 0), (-3., 0), (-3., 0)]),
case(array![1., 2., 3.] - array![4., 5., 6.], array_ma![(-3., 0), (-3., 0), (-3., 0)]),
case(array![1., 2., 3.] - array![4., 5., 6.], array_ma![(-3., 0), (-3., 0), (-3., 0)]),
case(array![[1., 2.], [3., 4.]] - array![[5., 6.], [7., 8.]], array![[-4., -4.], [-4., -4.]]),
case(array![[1., 2.], [3., 4.]] - array![[5., 6.], [7., 8.]], array_ma![[(-4., 0), (-4., 0)], [(-4., 0), (-4., 0)]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] - array![1., 2., 3., 4.], array![0.]),
case(array![[1., 2., 3.], [1., 2., 3.]] - array![1., 2., 3.], array![[0., 0., 0.], [0., 0., 0.]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] - array![1., 2.], array![[[0., 0.], [0., 0.]], [[0., 0.], [0., 0.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] - array![[1., 2.]], array![[[0., 0.], [0., 0.]], [[0., 0.], [0., 0.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] - array![[1., 2.], [1., 2.]], array![[[0., 0.], [0., 0.]], [[0., 0.], [0., 0.]]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] - array![2., 3., 4.], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] - array![[2., 3., 2., 4.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] - array![[2., 3., 2.], [4., 3., 2.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] - array![[[2., 3., 2.], [4., 3., 2.]]], array![0.]),
)] fn sub_test<S1: ArrayType, S2: ArrayType>(
    eq: impl ArrayImpl<S1>,
    expected: impl ArrayImpl<S2>
) {
    test_runner_gen(&eq, &expected)
}

#[rstest(
eq, expected,
case(array![1., 2., 3.] * 2, array![2., 4., 6.]),
case(array![1., 2., 3.] * 2., array![2., 4., 6.]),
case(2 * array![1., 2., 3.], array![2, 4, 6]),
case(2. * array![1., 2., 3.], array![2., 4., 6.]),
case(array![1., 2., 3.] * array![4., 5., 6.], array![4., 10., 18.]),
case(array![1., 2., 3.] * array![4., 5., 6.], array![4., 10., 18.]),
case(array![1., 2., 3.] * array![4., 5., 6.], array![4., 10., 18.]),
case(array![1., 2., 3.] * array![4., 5., 6.], array![4., 10., 18.]),
case(array![[1., 2.], [3., 4.]] * array![[5., 6.], [7., 8.]], array![[5., 12.], [21., 32.]]),
case(array![[1., 2.], [3., 4.]] * array![[5., 6.], [7., 8.]], array![[5., 12.], [21., 32.]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] * array![1., 2., 3., 4.], array![0.]),
case(array![[1., 2., 3.], [1., 2., 3.]] * array![2., 2., 2.], array![[2., 4., 6.], [2., 4., 6.]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] * array![2., 2.], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] * array![[2., 2.]], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] * array![[2., 2.], [2., 2.]], array![[[2., 4.], [2., 4.]], [[2., 4.], [2., 4.]]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] * array![2., 3., 4.], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] * array![[2., 3., 2., 4.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] * array![[2., 3., 2.], [4., 3., 2.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] * array![[[2., 3., 2.], [4., 3., 2.]]], array![0.]),
)] fn mul_test<S1: ArrayType, S2: ArrayType>(
    eq: impl ArrayImpl<S1>,
    expected: impl ArrayImpl<S2>
) {
    test_runner_gen(&eq, &expected)
}

#[rstest(
eq, expected,
case(array![1., 2., 3.] / 2, array![0.5, 1., 1.5]),
case(array![1., 2., 3.] / 2., array![0.5, 1., 1.5]),
case(2 / array![1., 2., 4.], array![2., 1., 0.5]),
case(2. / array![1., 2., 4.], array![2., 1., 0.5]),
case(array![1., 2., 3.] / array![4., 5., 6.], array![0.25, 0.4, 0.5]),
case(array![1., 2., 3.] / array![4., 5., 6.], array![0.25, 0.4, 0.5]),
case(array![1., 2., 3.] / array![4., 5., 6.], array![0.25, 0.4, 0.5]),
case(array![1., 2., 3.] / array![4., 5., 6.], array![0.25, 0.4, 0.5]),
case(array![[1., 2.], [3., 4.]] / array![[2., 4.], [6., 8.]], array![[0.5, 0.5], [0.5, 0.5]]),
case(array![[1., 2.], [3., 4.]] / array![[2., 4.], [6., 8.]], array![[0.5, 0.5], [0.5, 0.5]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] / array![1., 2., 3., 4.], array![0.]),
case(array![[1., 2., 3.], [1., 2., 3.]] / array![2., 2., 2.], array![[0.5, 1., 1.5], [0.5, 1., 1.5]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] / array![2., 2.], array![[[0.5, 1.], [0.5, 1.]], [[0.5, 1.], [0.5, 1.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] / array![[2., 2.]], array![[[0.5, 1.], [0.5, 1.]], [[0.5, 1.], [0.5, 1.]]]),
case(array![[[1., 2.], [1., 2.]], [[1., 2.], [1., 2.]]] / array![[2., 2.], [2., 2.]], array![[[0.5, 1.], [0.5, 1.]], [[0.5, 1.], [0.5, 1.]]]),
#[should_panic] case(array![[1., 2.], [3., 4.]] / array![2., 3., 4.], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] / array![[2., 3., 2., 4.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] / array![[2., 3., 2.], [4., 3., 2.]], array![0.]),
#[should_panic] case(array![[1., 2.], [3., 4.]] / array![[[2., 3., 2.], [4., 3., 2.]]], array![0.]),
)] fn div_test<S1: ArrayType, S2: ArrayType>(
    eq: impl ArrayImpl<S1>,
    expected: impl ArrayImpl<S2>
) {
    test_runner_gen(&eq, &expected)
}

#[rstest(
arr1, arr2, expected,
case(array![2., 2., 2.], array![2.], true),
case(array![1., 2., 3.], array![1., 2., 3.], true),
case(array![1., 2., 3.], array![2., 2., 3.], false),
case(array![[1., 2.], [3., 4.]], array![[1., 2.], [3., 4.]], true),
case(array![[1., 2.], [1., 2.]], array![[1., 2.]], true),
case(array![[1., 2.], [3., 4.]], array![[1., 2.], [1., 2.]], false),
)] fn equals_test<S1: ArrayType, S2: ArrayType>(
    arr1: Array<S1>,
    arr2: Array<S2>,
    expected: bool,
) {
    assert_eq!(arr1.equals(&arr2), expected);
}

#[rstest(
arr1, arr2, expected,
case(array![2., 2., 2.], array![2.], array![1., 1., 1.]),
case(array![1., 2., 3.], array![1., 2., 3.], array![1., 1., 1.]),
case(array![1., 2., 3.], array![2., 2., 3.], array![0., 1., 1.]),
case(array![[1., 2.], [3., 4.]], array![[1., 2.], [3., 4.]], array![[1., 1.], [1., 1.]]),
case(array![[1., 2.], [1., 2.]], array![[1., 2.]], array![[1., 1.], [1., 1.]]),
case(array![[1., 2.], [3., 4.]], array![[1., 2.], [1., 2.]], array![[1., 1.], [0., 0.]]),
)] fn equals_elems_test<S1: ArrayType, S2: ArrayType>(
    arr1: Array<S1>,
    arr2: Array<S2>,
    expected: Array<S1>,
) {
    assert_eq!(arr1.equals_elems(&arr2), expected);
}

#[rstest(
arr1, arr2, expected,
case(array![3.], array![4.], array![12.]),
case(array![2., 3.], array![2., 3.], array![13.]),
case(array![2., 3.], array![2.], array![4., 6.]),
case(array![[-2., -1., 0., 1., 2.], [-2., -1., 0.5, 2., 0.5]], array![[-2., -2.], [-1., -1.], [0., 0.5], [1., 2.], [2., 0.5]], array![[10., 8.], [8., 9.5]]),
)] fn dot_test<S1: ArrayType, S2: ArrayType>(
    arr1: Array<S1>,
    arr2: Array<S2>,
    expected: Array<S1>,
) {
    test_runner(&arr1.dot(arr2), &expected);
}

#[rstest(
arr1, arr2, expected,
case(array![3.], array![4.], array![12.]),
case(array![3., 3.], array![4., 4.], array![24.]),
case(array![[3., 3.]], array![[4.], [4.]], array![24.]),
case(array![[3.], [3.]], array![[4., 4.]], array![[12., 12.], [12., 12.]]),
case(array![[3., 3.], [4., 4.]], array![[4., 4.], [6., 6.]], array![[30., 30.], [40., 40.]]),
case(array![[-2., -1., 0., 1., 2.], [-2., -1., 0.5, 2., 0.5]], array![[-2., -2.], [-1., -1.], [0., 0.5], [1., 2.], [2., 0.5]], array![[10., 8.], [8., 9.5]]),
)] fn matmul_test<S1: ArrayType, S2: ArrayType>(
    arr1: Array<S1>,
    arr2: Array<S2>,
    expected: Array<S1>,
) {
    test_runner(&arr1.matmul(arr2), &expected);
}
