use rstest::rstest;

use numrs::prelude::*;

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), 1),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), 2),
case(array_1d(vec![4., 6.]), 1),
case(array_2d(vec![4., 6., 3., 6.], (2, 2)), 2),
case(array_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2)), 3),
)] fn create_array_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.ndim(), expected); }

#[rstest(
arr, expected,
case(array![4., 6.], 1),
case(array![[4., 6.], [4., 6.]], 2),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], 3),
)] fn create_array_macro_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.ndim(), expected); }

#[rstest(
range, step, expected,
case(0..5, None, array![0., 1., 2., 3., 4.]),
case(0..5, Some(1), array![0., 1., 2., 3., 4.]),
case(0..5, Some(2), array![0., 2., 4.]),
)] fn arange_test(range: std::ops::Range<usize>, step: Option<usize>, expected: Array<f64>) { assert_eq!(Array::<f64>::arange(range, step), expected); }

#[test] fn get_elements_test() {
    assert_eq!(array![4., -6.].get_elements(), vec![4., -6.].iter().map(|i| i.to_elem()).collect::<Vec<ArrayElem<f64>>>())
}

#[test] fn get_elements_vec_test() {
    assert_eq!(array![4., -6.].get_elements_vec(), vec![4., -6.])
}

#[test] fn get_shape_test() {
    assert_eq!(array![4., -6.].get_shape(), vec![2])
}

#[rstest(
arr, expected,
case(array![1., 2., 3.], array![1., 2., 3.]),
case(array![[1., 2.], [3., 4.]], array![1., 2., 3., 4.]),
case(array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]], array![1., 2., 3., 4., 1., 2., 3., 4.]),
)] fn ravel_test(arr: Array<f64>, expected: Array<f64>) { assert_eq!(arr.ravel(), expected) }

#[rstest(
arr, shape, expected,
case(array![1., 2., 3.], vec![2, 3], array![[1., 2., 3.], [1., 2., 3.]]),
case(array![[1., 2.], [3., 4.]], vec![2, 2, 2], array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]]),
)] fn broadcast_test(arr: Array<f64>, shape: Vec<usize>, expected: Array<f64>) { assert_eq!(arr.broadcast_to(shape), expected) }

#[rstest(
arr, expected,
case(array_1d(vec![1., 2., 3.]), false),
case(array_2d(vec![1., 2., 3., 4.], (2, 2)), false),
case(array_2d(vec![f64::NAN, 2., 3., f64::NAN], (2, 2)), true),
)] fn contains_nan_test(arr: Array<f64>, expected: bool) { assert_eq!(arr.contains_nan(), expected) }

#[rstest(
arr, expected,
case(array_1d(vec![1., 2., 3.]), 3),
case(array_2d(vec![1., 2., 3., 4.], (2, 2)), 4),
case(array_2d(vec![f64::NAN, 2., 3., f64::NAN], (2, 2)), 4),
)] fn values_count_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.values_len(), expected) }

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), vec![4., 6.]),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), vec![4., 6., 4., 6.]),
case(array_1d(vec![4., 6.]), vec![4., 6.]),
case(array_2d(vec![4., 6., 3., 6.], (2, 2)), vec![4., 6., 3., 6.]),
case(array_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2)), vec![4., 6., 3., 6., 4., 6., 3., 6.]),
)] fn iter_test(arr: Array<f64>, expected: Vec<f64>) { assert_eq!(arr.into_iter().collect::<Vec<f64>>(), expected); }

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), Array::new(vec![16., 36.], vec![2])),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), Array::new(vec![16., 36., 16., 36.], vec![2, 2])),
case(array_1d(vec![4., 6.]), Array::new(vec![16., 36.], vec![2])),
)] fn map_test(arr: Array<f64>, expected: Array<f64>) { assert_eq!(arr.map(|i| *i * *i), expected); }

#[rstest(
arr, offset, expected,
case(array![1.], None, array![1.]),
case(array![1., 2.], None, array![[1., 0.], [0., 2.]]),
case(array![1., 2., 3.], None, array![[1., 0., 0.], [0., 2., 0.], [0., 0., 3.]]),
case(array![1., 2., 3.], Some(1), array![[0., 1., 0., 0.], [0., 0., 2., 0.], [0., 0., 0., 3.], [0., 0., 0., 0.]]),
case(array![[1., 2.], [3., 4.]], None, array![1., 4.]),
case(array![[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]], None, array![1., 5., 9.]),
case(array![[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]], Some(1), array![2., 6.]),
case(array![[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]], Some(4), Array::empty()),
case(array![[2.5, 2.], [2., 2.375]], None, array![2.5, 2.375]),
)] fn diag_test(arr: Array<f64>, offset: Option<usize>, expected: Array<f64>) { assert_eq!(arr.diag(offset), expected); }

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), vec![2]),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), vec![2, 2]),
case(array_1d(vec![4., 6.]), vec![2]),
case(array_2d(vec![4., 6., 3., 6.], (2, 2)), vec![2, 2]),
case(array_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2)), vec![2, 2, 2]),
)] fn shape_test(arr: Array<f64>, expected: Vec<usize>) { assert_eq!(arr.get_shape(), expected); }

#[rstest(
arr, index, expected,
case(Array::new(gen_e(4), gen_s(2)), vec![0, 1], 2.),
case(Array::new(gen_e(8), gen_s(3)), vec![0, 1, 1], 4.),
case(Array::new(gen_e(16), gen_s(4)), vec![0, 1, 0, 1], 6.),
case(Array::new(gen_e(32), gen_s(5)), vec![0, 1, 0, 1, 0], 11.),
)] fn get_element_at_test(arr: Array<f64>, index: Vec<usize>, expected: f64) { assert_eq!(arr.at_v(index), expected.to_elem()); }
fn gen_e(count: usize) -> Vec<f64> { (0..count).map(|i| (i + 1).to_float()).collect() }
fn gen_s(count: usize) -> Vec<usize> { (0..count).map(|_| 2).collect() }

#[rstest(
arr, axes, expected,
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![0, 1, 2]), array![[[1.0000, 2.0000], [3.0000, 4.0000]], [[5.0000, 6.0000], [7.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![0, 2, 1]), array![[[1.0000, 3.0000], [2.0000, 4.0000]], [[5.0000, 7.0000], [6.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![1, 0, 2]), array![[[1.0000, 2.0000], [5.0000, 6.0000]], [[3.0000, 4.0000], [7.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![1, 2, 0]), array![[[1.0000, 5.0000], [2.0000, 6.0000]], [[3.0000, 7.0000], [4.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![2, 0, 1]), array![[[1.0000, 3.0000], [5.0000, 7.0000]], [[2.0000, 4.0000], [6.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![2, 1, 0]), array![[[1.0000, 5.0000], [3.0000, 7.0000]], [[2.0000, 6.0000], [4.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], None, array![[[1.0000, 5.0000], [3.0000, 7.0000]], [[2.0000, 6.0000], [4.0000, 8.0000]]]),
)] fn transpose_test(arr: Array<f64>, axes: Option<Vec<usize>>, expected: Array<f64>) {
    assert_eq!(arr.transpose(axes), expected)
}

#[rstest(
arr1, arr2, expected,
case(array![0, 1, 0], array![1, 2, 0], array![2, 0, 1]),
case(array![0, 1, 0], array![0, 2, 1], array![0, 2, 1]),
case(array![2, 4, 6, 3, 7, 5], array![8, 3, 6, 4, 5, 2], array![5, 1, 3, 4, 2, 0]),
case(array![9, 3, 1, 7, 4, 3, 6], array![4, 6, 9, 2, 1, 8, 7], array![4, 3, 0, 1, 6, 5, 2]),
case(array![1, 5, 1, 4, 3, 4, 4], array![9, 4, 0, 4, 0, 2, 1], array![2, 4, 6, 5, 3, 1, 0]),
)] fn lexsort_test(arr1: Array<usize>, arr2: Array<usize>, expected: Array<usize>) {
    assert_eq!(arr1.lexsort(&arr2), expected)
}
