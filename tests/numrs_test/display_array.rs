use rstest::rstest;

use numrs::prelude::*;

#[rstest(
arr,
case(array![4.]),
case(array![4., 6.]),
case(array![[4., 6.], [3., 7.]]),
case(array![[4., 6., 2., 8.], [3., 7., 9., 5.], [3., 7., 8., 7.]])
)] fn display_simple_test(arr: Array<f64>) { println!("{}", arr) }

#[rstest(
arr, expected,
case(array![1., 2., 3., 4.], "[1.0000, 2.0000, 3.0000, 4.0000]"),
case(array![[1., 2.], [3., 4.]], "[[1.0000, 2.0000], [3.0000, 4.0000]]"),
case(array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]], "[[[1.0000, 2.0000], [3.0000, 4.0000]], [[1.0000, 2.0000], [3.0000, 4.0000]]]"),
)] fn display_test(arr: Array<f64>, expected: &str) { assert_eq!(&format!("{}", arr), expected) }
