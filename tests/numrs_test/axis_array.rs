use rstest::rstest;

use numrs::prelude::*;

use crate::numrs_test::common::{arr_fv, test_runner};

#[rstest(
arr, source, destination, expected,
case(array![1., 2.], vec![0], vec![0], array![1., 2.]),
case(array![[1., 2.], [3., 4.]], vec![0], vec![0], array![[1., 2.], [3., 4.]]),
case(array![[1., 2.], [3., 4.]], vec![0], vec![1], array![[1., 3.], [2., 4.]]),
case(array![[1., 2.], [3., 4.]], vec![1], vec![0], array![[1., 3.], [2., 4.]]),
case(array![[1., 2.], [3., 4.]], vec![1], vec![1], array![[1., 2.], [3., 4.]]),
case(array![[1., 2.], [3., 4.]], vec![0, 1], vec![-1, -2], array![[1., 3.], [2., 4.]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![0], vec![0], array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![0], vec![1], array![[[1., 2.], [5., 6.]], [[3., 4.], [7., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![0], vec![2], array![[[1., 5.], [2., 6.]], [[3., 7.], [4., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![1], vec![0], array![[[1., 2.], [5., 6.]], [[3., 4.], [7., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![1], vec![1], array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![1], vec![2], array![[[1., 3.], [2., 4.]], [[5., 7.], [6., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![2], vec![0], array![[[1., 3.], [5., 7.]], [[2., 4.], [6., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![2], vec![1], array![[[1., 3.], [2., 4.]], [[5., 7.], [6., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![2], vec![2], array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![0, 1], vec![-1, -2], array![[[1., 5.], [3., 7.]], [[2., 6.], [4., 8.]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], vec![0, 1, 2], vec![-1, -2, -3], array![[[1., 5.], [3., 7.]], [[2., 6.], [4., 8.]]]),
)] fn moveaxis_test(arr: Array<f64>, source: Vec<isize>, destination: Vec<isize>, expected: Array<f64>) {
    assert_eq!(arr.moveaxis(source, destination), expected)
}

#[rstest(
arr, indices, axis, expected,
case(array![5., 4., 1., 0., -1., -3., -4., 0.], vec![0, 0], 0, array![5., 5.]),
case(array![5., 4., 1., 0., -1., -3., -4., 0.], vec![1, 2], 0, array![4., 1.]),
case(array![5., 4., 1., 0., -1., -3., -4., 0.], vec![4, 6], 0, array![-1., -4.]),
case(array![[5., 4., 1.], [0., -1., -3.]], vec![0, 1], 0, array![[5., 4., 1.], [0., -1., -3.]]),
case(array![[5., 4., 1.], [0., -1., -3.]], vec![0, 1], 1, array![[5., 4.], [0., -1.]]),
case(array![[[5., 4., 1.], [0., -1., -3.]], [[1., 2., 3.], [3., 4., 5.]]], vec![0, 1], 0, array![[[5., 4., 1.], [0., -1., -3.]], [[1., 2., 3.], [3., 4., 5.]]]),
case(array![[[5., 4., 1.], [0., -1., -3.]], [[1., 2., 3.], [3., 4., 5.]]], vec![0, 1], 1, array![[[5., 4., 1.], [0., -1., -3.]], [[1., 2., 3.], [3., 4., 5.]]]),
case(array![[[5., 4., 1.], [0., -1., -3.]], [[1., 2., 3.], [3., 4., 5.]]], vec![0, 1], 2, array![[[5., 4.], [0., -1.]], [[1., 2.], [3., 4.]]]),
)] fn take_test(arr: Array<f64>, indices: Vec<usize>, axis: usize, expected: Array<f64>) {
    assert_eq!(arr.take(indices.as_slice(), axis), expected)
}

#[rstest(
arr, axis, expected,
case(array![5., 7., 1., 8., 3., 4., 2., 6.], None, array![1., 2., 3., 4., 5., 6., 7., 8.]),
case(array![[5., 7., 1., 8.], [3., 4., 2., 6.]], None, array![[1., 5., 7., 8.], [2., 3., 4., 6.]]),
case(array![[5., 7., 1., 8.], [3., 4., 2., 6.]], Some(0), array![[3., 4., 1., 6.], [5., 7., 2., 8.]]),
case(array![[5., 7., 1., 8.], [3., 4., 2., 6.]], Some(1), array![[1., 5., 7., 8.], [2., 3., 4., 6.]]),
case(array![[[5., 7., 1.], [3., 4., 2.]], [[4., 5., 6.], [7., 2., 4.]]], Some(0), array![[[4., 5., 1.], [3., 2., 2.]], [[5., 7., 6.], [7., 4., 4.]]]),
case(array![[[5., 7., 1.], [3., 4., 2.]], [[4., 5., 6.], [7., 2., 4.]]], Some(1), array![[[3., 4., 1.], [5., 7., 2.]], [[4., 2., 4.], [7., 5., 6.]]]),
case(array![[[5., 7., 1.], [3., 4., 2.]], [[4., 5., 6.], [7., 2., 4.]]], Some(2), array![[[1., 5., 7.], [2., 3., 4.]], [[4., 5., 6.], [2., 4., 7.]]]),
)] fn sort_test(arr: Array<f64>, axis: Option<usize>, expected: Array<f64>) { assert_eq!(arr.sort(axis), expected) }

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), 10.),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), 20.),
case(array_1d(vec![4., 6.]), 10.),
case(array_2d(vec![4., 6., 3., 6.], (2, 2)), 19.),
case(array_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2)), 38.),
)] fn sum_test(arr: Array<f64>, expected: f64) { assert_eq!(arr.sum(None, None).single(), expected.to_elem()); }

#[rstest(
arr, axis, expected,
case(array![1., 2.], 0, array![3.]),
case(array![[1., 2.], [3., 4.]], 0, array![4., 6.]),
case(array![[1., 2.], [3., 4.]], 1, array![3., 7.]),
case(array![[1., 2., 3.], [3., 4., 5.]], 0, array![4., 6., 8.]),
case(array![[1., 2.], [3., 3.], [4., 5.]], 0, array![8., 10.]),
case(array![[1., 2., 3.], [3., 4., 5.]], 1, array![6., 12.]),
case(array![[1., 2.], [3., 3.], [4., 5.]], 1, array![3., 6., 9.]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, array![[2., 4., 6., 8.], [10., 12., 14., 16.]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, array![[6., 8., 10., 12.], [6., 8., 10., 12.]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, array![[10., 26.], [10., 26.]]),
)] fn sum_axis_test(arr: Array<f64>, axis: usize, expected: Array<f64>) { assert_eq!(arr.sum(Some(axis), None), expected); }

#[rstest(
arr, axis, ddof, expected,
case(array![1., 2.], 0, 0, array![0.5]),
case(array![1., 2.], 0, 1, array![std::f64::consts::FRAC_1_SQRT_2]),
case(array![[1., 2.], [3., 4.]], 0, 0, arr_fv(1., vec![2])),
case(array![[1., 2.], [3., 4.]], 0, 1, arr_fv(std::f64::consts::SQRT_2, vec![2])),
case(array![[1., 2.], [3., 4.]], 1, 0, arr_fv(0.5, vec![2])),
case(array![[1., 2.], [3., 4.]], 1, 1, arr_fv(std::f64::consts::FRAC_1_SQRT_2, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 0, 0, arr_fv(1.247219128924647, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 0, 1, arr_fv(1.5275252316519465, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 1, 0, array![0.5, 0., 0.5]),
case(array![[1., 2.], [3., 3.], [4., 5.]], 1, 1, array![std::f64::consts::FRAC_1_SQRT_2, 0., std::f64::consts::FRAC_1_SQRT_2]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 0, arr_fv(0., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 1, arr_fv(0., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 2, arr_fv(f64::NAN, vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 0, arr_fv(2., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 1, arr_fv(2.8284271247461903, vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 2, arr_fv(f64::INFINITY, vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 0, arr_fv(1.118033988749895, vec![2, 2])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 1, arr_fv(1.2909944487358056, vec![2, 2])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 2, arr_fv(1.5811388300841898, vec![2, 2])),
)] fn std_axis_test(arr: Array<f64>, axis: usize, ddof: usize, expected: Array<f64>) {
    test_runner(&arr.std(Some(axis), ddof, None), &expected);
}

#[rstest(
arr, axis, ddof, expected,
case(array![1., 2.], 0, 0, array![0.25]),
case(array![1., 2.], 0, 1, array![0.5]),
case(array![[1., 2.], [3., 4.]], 0, 0, arr_fv(1., vec![2])),
case(array![[1., 2.], [3., 4.]], 0, 1, arr_fv(2., vec![2])),
case(array![[1., 2.], [3., 4.]], 1, 0, arr_fv(0.25, vec![2])),
case(array![[1., 2.], [3., 4.]], 1, 1, arr_fv(0.5, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 0, 0, arr_fv(1.5555555555555554, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 0, 1, arr_fv(2.333333333333333, vec![2])),
case(array![[1., 2.], [3., 3.], [4., 5.]], 1, 0, array![0.25, 0., 0.25]),
case(array![[1., 2.], [3., 3.], [4., 5.]], 1, 1, array![0.5, 0., 0.5]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 0, arr_fv(0., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 1, arr_fv(0., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, 2, arr_fv(f64::NAN, vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 0, arr_fv(4., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 1, arr_fv(8., vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, 2, arr_fv(f64::INFINITY, vec![2, 4])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 0, arr_fv(1.25, vec![2, 2])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 1, arr_fv(1.6666666666666667, vec![2, 2])),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, 2, arr_fv(2.5, vec![2, 2])),
)] fn var_axis_test(arr: Array<f64>, axis: usize, ddof: usize, expected: Array<f64>) {
    test_runner(&arr.var(Some(axis), ddof, None), &expected);
}

#[rstest(
arr, expected,
case(array![1., 2.], array![2]),
case(array![[1., 2.], [3., 4.]], array![4]),
case(array![[1., 2.], [3., 4.]], array![4]),
)] fn count_test(arr: Array<f64>, expected: Array<usize>) { assert_eq!(arr.count(None, None), expected) }

#[rstest(
arr, axis, expected,
case(array![1., 2.], 0, array![2]),
case(array![[1., 2.], [3., 4.]], 0, array![2, 2]),
case(array![[1., 2.], [3., 4.]], 1, array![2, 2]),
case(array![[1., 2., 3.], [2., 3., 4.]], 0, array![2, 2, 2]),
case(array![[1., 2., 3.], [2., 3., 4.]], 1, array![3, 3]),
case(array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]], 0, array![[2, 2], [2, 2]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 0, array![[2, 2, 2, 2], [2, 2, 2, 2]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 1, array![[2, 2, 2, 2], [2, 2, 2, 2]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], 2, array![[4, 4], [4, 4]]),
)] fn count_axis_test(arr: Array<f64>, axis: usize, expected: Array<usize>) { assert_eq!(arr.count(Some(axis), None), expected) }

#[rstest(
arr, axis, expected,
case(array![[10., 7., 4.], [3., 2., 1.]], None, array![3.5]),
case(array![[10., 7., 4.], [3., 2., 1.]], Some(0), array![6.5, 4.5, 2.5]),
case(array![[10., 7., 4.], [3., 2., 1.]], Some(1), array![7., 2.]),
)] fn median_test(arr: Array<f64>, axis: Option<usize>, expected: Array<f64>) { assert_eq!(arr.median(axis, None), expected); }

#[rstest(
arr, expected,
case(Array::new(vec![4., 6.], vec![2]), 5.),
case(Array::new(vec![4., 6., 4., 6.], vec![2, 2]), 5.),
case(array_1d(vec![4., 6.]), 5.),
case(array_2d(vec![4., 6., 4., 6.], (2, 2)), 5.),
case(array_3d(vec![4., 6., 4., 6., 4., 6., 4., 6.], (2, 2, 2)), 5.),
)] fn mean_test(arr: Array<f64>, expected: f64) { assert_eq!(arr.mean(None, None).single(), expected.to_elem()); }

#[rstest(
arr, axis, keepdims, expected,
case(array![1., 2.], None, Some(true), array![1.5]),
case(array![[1., 2.], [3., 4.]], None, None, array![2.5]),
case(array![[1., 2.], [3., 4.]], Some(0), Some(false), array![2., 3.]),
case(array![[1., 2.], [3., 4.]], Some(1), Some(true), array![[1.5], [3.5]]),
case(array![[1., 2., 3.], [2., 3., 4.]], Some(0), None, array![1.5, 2.5, 3.5]),
case(array![[1., 2., 3.], [2., 3., 4.]], Some(1), None, array![2., 3.]),
case(array![[1., 2., 3.], [2., 3., 4.]], Some(0), Some(true), array![[1.5, 2.5, 3.5]]),
case(array![[1., 2., 3.], [2., 3., 4.]], Some(1), Some(true), array![[2.], [3.]]),
case(array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]], None, None, array![2.5]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], None, None, array![4.5]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], Some(0), None, array![[1., 2., 3., 4.], [5., 6., 7., 8.]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], Some(0), Some(true), array![[[1., 2., 3., 4.], [5., 6., 7., 8.]]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], Some(1), None, array![[3., 4., 5., 6.], [3., 4., 5., 6.]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], Some(2), None, array![[2.5, 6.5], [2.5, 6.5]]),
case(array![[[1., 2., 3., 4.], [5., 6., 7., 8.]], [[1., 2., 3., 4.], [5., 6., 7., 8.]]], Some(2), Some(true), array![[[2.5], [6.5]], [[2.5], [6.5]]]),
)] fn mean_axis_test(arr: Array<f64>, axis: Option<usize>, keepdims: Option<bool>, expected: Array<f64>) { assert_eq!(arr.mean(axis, keepdims), expected) }

#[rstest(
arr, expected,
case(array![4., 6.], array![2.]),
case(array![4., 6., 4., 6.], array_ma![(2., 0), (-2., 0), (2., 0)]),
case(array![4., 6., 4., 6., 4., 6., 4., 6.], array_ma![(2., 0), (-2., 0), (2., 0), (-2., 0), (2., 0), (-2., 0), (2., 0)]),
)] fn diff_test(arr: Array<f64>, expected: Array<f64>) {
    test_runner(&arr.diff(None), &expected);
}

#[rstest(
arr, axis, expected,
case(array![[4., 6.], [4., 6.]], None, array![[2.], [2.]]),
case(array![[4., 6.], [4., 6.]], Some(0), array![[0., 0.]]),
case(array![[4., 6.], [4., 6.]], Some(1), array![[2.], [2.]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], None, array![[[2.], [2.]], [[2.], [2.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(0), array![[[0., 0.], [0., 0.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(1), array![[[0., 0.]], [[0., 0.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(2), array![[[2.], [2.]], [[2.], [2.]]]),
)] fn diff_axis_test(arr: Array<f64>, axis: Option<usize>, expected: Array<f64>) {
    test_runner(&arr.diff(axis), &expected);
}

#[rstest(
arr, expected,
case(array![4., 6.], array![4., 10.]),
case(array![4., 6., 4., 6.], array![4., 10., 14., 20.]),
case(array![4., 6., 4., 6., 4., 6., 4., 6.], array![4., 10., 14., 20., 24., 30., 34., 40.]),
)] fn cumsum_test(arr: Array<f64>, expected: Array<f64>) {
    test_runner(&arr.cumsum(None), &expected);
}

#[rstest(
arr, axis, expected,
case(array![[4., 6.], [4., 6.]], None, array![4., 10., 14., 20.]),
case(array![[4., 6.], [4., 6.]], Some(0), array![[4., 6.], [8., 12.]]),
case(array![[4., 6.], [4., 6.]], Some(1), array![[4., 10.], [4., 10.]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], None, array![4., 10., 14., 20., 24., 30., 34., 40.]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(0), array![[[4., 6.], [4., 6.]], [[8., 12.], [8., 12.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(1), array![[[4., 6.], [8., 12.]], [[4., 6.], [8., 12.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(2), array![[[4., 10.], [4., 10.]], [[4., 10.], [4., 10.]]]),
)] fn cumsum_axis_test(arr: Array<f64>, axis: Option<usize>, expected: Array<f64>) {
    test_runner(&arr.cumsum(axis), &expected);
}

#[rstest(
arr, expected,
case(array![4., 6.], array![0., 1.]),
case(array![4., 6., 4., 6.], array![0., 2., 1., 3.]),
case(array![4., 6., 4., 6., 4., 6., 4., 6.], array![0., 2., 4., 6., 1., 3., 5., 7.]),
)] fn argsort_test(arr: Array<f64>, expected: Array<f64>) {
    test_runner(&arr.argsort(None), &expected);
}

#[rstest(
arr, axis, expected,
case(array![[4., 6.], [4., 6.]], None, array![[0., 1.], [0., 1.]]),
case(array![[4., 6.], [4., 6.]], Some(0), array![[0., 0.], [1., 1.]]),
case(array![[4., 6.], [4., 6.]], Some(1), array![[0., 1.], [0., 1.]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], None, array![[[0., 1.], [0., 1.]], [[0., 1.], [0., 1.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(0), array![[[0., 0.], [0., 0.]], [[1., 1.], [1., 1.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(1), array![[[0., 0.], [1., 1.]], [[0., 0.], [1., 1.]]]),
case(array![[[4., 6.], [4., 6.]], [[4., 6.], [4., 6.]]], Some(2), array![[[0., 1.], [0., 1.]], [[0., 1.], [0., 1.]]]),
)] fn argsort_axis_test(arr: Array<f64>, axis: Option<usize>, expected: Array<f64>) {
    test_runner(&arr.argsort(axis), &expected);
}

#[rstest(
arr, arrs, axis, expected,
case(array![1., 2., 3.], &[array![4., 5., 6.]], None, array![1., 2., 3., 4., 5., 6.]),
case(_carr(), &[_carr(), _carr()], None, array![1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5., 1., 2., 1., 2., 1., 3., 3., 5.]),
case(_carr(), &[_carr(), _carr()], Some(0), array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]], [[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]]),
case(_carr(), &[_carr(), _carr()], Some(1), array![[[1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.], [1., 2.]], [[1., 3.], [3., 5.], [1., 3.], [3., 5.], [1., 3.], [3., 5.]]]),
case(_carr(), &[_carr(), _carr()], Some(2), array![[[1., 2., 1., 2., 1., 2.], [1., 2., 1., 2., 1., 2.]], [[1., 3., 1., 3., 1., 3.], [3., 5., 3., 5., 3., 5.]]]),
)] fn concatenate_test(arr: Array<f64>, arrs: &[Array<f64>], axis: Option<usize>, expected: Array<f64>) {
    test_runner(&arr.concatenate(arrs, axis), &expected);
}

fn _carr() -> Array<f64> {
    array![[[1., 2.], [1., 2.]], [[1., 3.], [3., 5.]]]
}

#[rstest(
arr, ord, axis, expected,
case(_narr_a(), None, None, Array::new_flat(vec![7.745966692414834])),
case(_narr_b(), None, None, Array::new_flat(vec![7.745966692414834])),
case(_narr_b(), Some(Ord::frobenius()), None, Array::new_flat(vec![7.745966692414834])),
case(_narr_a(), Some(Ord::numeric(f64::inf())), None, Array::new_flat(vec![4.])),
case(_narr_a(), Some(Ord::numeric(f64::neg_inf())), None, Array::new_flat(vec![0.])),
case(_narr_a(), Some(Ord::numeric(1.)), None, Array::new_flat(vec![20.])),
// case(_narr_b(), Some(Ord::numeric(1.)), None, Array::new_flat(vec![7.])),
case(_narr_a(), Some(Ord::numeric(-1.)), None, Array::new_flat(vec![0.])),
// case(_narr_b(), Some(Ord::numeric(-1.)), None, Array::new_flat(vec![6.])),
case(_narr_a(), Some(Ord::numeric(2.)), None, Array::new_flat(vec![7.745966692414834])),
// case(_narr_b(), Some(Ord::numeric(2.)), None, Array::new_flat(vec![7.3484692283495345])),
case(_narr_a(), Some(Ord::numeric(-2.)), None, Array::new_flat(vec![0.])),
// case(_narr_b(), Some(Ord::numeric(-2.)), None, Array::new_flat(vec![1.857_033_188_519_056_3e-16])),
case(_narr_a(), Some(Ord::numeric(3.)), None, Array::new_flat(vec![5.848_035_476_425_731])),
// case(_narr_b(), Some(Ord::numeric(3.)), None, Array::new_flat(vec![0.])),
case(_narr_c(), None, Some(0), Array::new_flat(vec![std::f64::consts::SQRT_2, 2.23606797749979, 5.])),
case(_narr_c(), None, Some(1), Array::new_flat(vec![3.7416573867739413, 4.242640687119285])),
case(_narr_c(), Some(Ord::new(1.)), Some(1), Array::new_flat(vec![6., 6.])),
)] fn norm_test(arr: Array<f64>, ord: Option<Ord<f64>>, axis: Option<usize>, expected: Array<f64>) {
    test_runner(&arr.norm(ord, axis, None), &expected);
}

fn _narr_a() -> Array<f64> {
    Array::arange(0 .. 9, None).map(|i| *i - (4.).to_elem())
}

fn _narr_b() -> Array<f64> {
    _narr_a().update_shape(vec![3, 3])
}

fn _narr_c() -> Array<f64> {
    Array::new(vec![1., 2., 3., -1., 1., 4.], vec![2, 3])
}
