use std::cmp::Ordering;

use rstest::rstest;

use numrs::ext::primitives::{FloatCmp, Not};

#[rstest(
arg, exp,
case(true, false),
case(false, true),
)] fn not_test(arg: bool, exp: bool) { assert_eq!(arg.not(), exp) }

#[rstest(
arg, exp,
case(0., 0.),
case(1.23, 1.23),
case(1.342234224, 1.342234224),
#[should_panic] case(0., 1.),
#[should_panic] case(1.3, 0.2),
)] fn approx_eq_test(arg: f64, exp: f64) { assert!(arg.approx_eq(exp)) }

#[rstest(
arg1, arg2, exp,
case(0., 0., Ordering::Equal),
case(1.23, 1.23, Ordering::Equal),
case(1.342234224, 1.342234224, Ordering::Equal),
case(0., 1., Ordering::Less),
case(1.3, 0.2, Ordering::Greater),
)] fn approx_eq_ord_test(arg1: f64, arg2: f64, exp: Ordering) { assert_eq!(arg1.approx_eq_ord(arg2), exp) }
