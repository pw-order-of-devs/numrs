use rstest::rstest;

use numrs::prelude::*;

#[rstest(
arr, expected,
case(Array::new_masked(vec![4., 6.], vec![2], vec![0, 0]), 1),
case(Array::new_masked(vec![4., 6., 4., 6.], vec![2, 2], vec![0, 0, 0, 0]), 2),
case(array_ma_1d(vec![4., 6.], vec![0, 0]), 1),
case(array_ma_2d(vec![4., 6., 3., 6.], (2, 2), vec![0, 0, 0, 0]), 2),
case(array_ma_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2), vec![0, 0, 0, 0, 0, 0, 0, 0]), 3),
)] fn create_array_ma_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.ndim(), expected); }

#[rstest(
arr, expected,
case(array_ma![(4.,0), (6.,0)], 1),
case(array_ma![[(4.,0), (6.,0)], [(4.,0), (6.,0)]], 2),
case(array_ma![[[(4.,0), (6.,0)], [(4.,0), (6.,0)]], [[(4.,0), (6.,0)], [(4.,0), (6.,0)]]], 3),
)] fn create_array_ma_macro_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.ndim(), expected); }

#[rstest(
arr, expected,
case(array![4., -6.], 1),
case(array![[-4., 6.], [4., -6.]], 2),
case(array![[[4., -6.], [-4., 6.]], [[-4., 6.], [4., -6.]]], 3),
)] fn create_array_macro_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.ndim(), expected); }

#[test] fn get_elements_test() {
    assert_eq!(array![4., -6.].get_elements(), vec![(4.).to_elem(), (-6.).to_elem()])
}

#[test] fn get_shape_test() {
    assert_eq!(array![4., -6.].get_shape(), vec![2])
}

#[rstest(
arr, expected,
case(array![1., 2., 3.], array![1., 2., 3.]),
case(array![[1., 2.], [3., 4.]], array![1., 2., 3., 4.]),
case(array![[[1., 2.], [3., 4.]], [[1., 2.], [3., 4.]]], array![1., 2., 3., 4., 1., 2., 3., 4.]),
)] fn ravel_test(arr: Array<f64>, expected: Array<f64>) { assert_eq!(arr.ravel(), expected) }

#[rstest(
arr, expected,
case(array_ma_1d(vec![1., 2., 3.], vec![0, 0, 0]), false),
case(array_ma_2d(vec![1., 2., 3., 4.], (2, 2), vec![0, 0, 0, 0]), false),
case(array_ma_2d(vec![f64::NAN, 2., 3., f64::NAN], (2, 2), vec![0, 0, 0, 0]), true),
)] fn contains_nan_test(arr: Array<f64>, expected: bool) { assert_eq!(arr.contains_nan(), expected) }

#[rstest(
arr, expected,
case(array_ma_1d(vec![1., 2., 3.], vec![0, 0, 0]), 3),
case(array_ma_2d(vec![1., 2., 3., 4.], (2, 2), vec![0, 0, 0, 0]), 4),
case(array_ma_2d(vec![f64::NAN, 2., 3., f64::NAN], (2, 2), vec![0, 0, 0, 0]), 4),
case(array_ma_2d(vec![f64::NAN, 2., 3., f64::NAN], (2, 2), vec![1, 0, 0, 1]), 2),
)] fn values_count_test(arr: Array<f64>, expected: usize) { assert_eq!(arr.values_len(), expected) }

#[rstest(
arr, expected,
case(Array::new_masked(vec![4., 6.], vec![2], vec![0, 0]), vec![(4.).to_elem(), (6.).to_elem()]),
case(Array::new_masked(vec![4., 6., 4., 6.], vec![2, 2], vec![0, 0, 0, 0]), vec![(4.).to_elem(), (6.).to_elem(), (4.).to_elem(), (6.).to_elem()]),
case(array_ma_1d(vec![4., 6.,], vec![0, 0]), vec![(4.).to_elem(), (6.).to_elem()]),
case(array_ma_2d(vec![4., 6., 3., 6.], (2, 2), vec![0, 0, 0, 0]), vec![(4.).to_elem(), (6.).to_elem(), (3.).to_elem(), (6.).to_elem()]),
)] fn iter_test(arr: Array<f64>, expected: Vec<ArrayElem<f64>>) { assert_eq!(arr.into_iter().collect::<Vec<ArrayElem<f64>>>(), expected); }

#[rstest(
arr, expected,
case(Array::new_masked(vec![4., 6.], vec![2], vec![0, 0]), Array::new_masked(vec![16., 36.], vec![2], vec![0, 0])),
case(Array::new_masked(vec![4., 6., 4., 6.], vec![2, 2], vec![0, 0, 0, 0]), Array::new_masked(vec![16., 36., 16., 36.], vec![2, 2], vec![0, 0, 0, 0])),
case(Array::new_masked(vec![4., 6., 4., 6.], vec![2, 2], vec![0, 0, 1, 1]), Array::new_masked(vec![16., 36., 4., 6.], vec![2, 2], vec![0, 0, 1, 1])),
case(array_ma_1d(vec![4., 6.], vec![0, 0]), Array::new_masked(vec![16., 36.], vec![2], vec![0, 0])),
case(array_ma_1d(vec![4., 6.], vec![0, 1]), Array::new_masked(vec![16., 6.], vec![2], vec![0, 1])),
)] fn map_test(arr: Array<f64>, expected: Array<f64>) { assert_eq!(arr.map(|i| *i * *i), expected); }

#[rstest(
arr, expected,
case(Array::new_masked(vec![4., 6.], vec![2], vec![0, 0]), vec![2]),
case(Array::new_masked(vec![4., 6., 4., 6.], vec![2, 2], vec![0, 0, 0, 0]), vec![2, 2]),
case(array_ma_1d(vec![4., 6.], vec![0, 0]), vec![2]),
case(array_ma_2d(vec![4., 6., 3., 6.], (2, 2), vec![0, 0, 0, 0]), vec![2, 2]),
case(array_ma_3d(vec![4., 6., 3., 6., 4., 6., 3., 6.], (2, 2, 2), vec![0, 0, 0, 0, 0, 0, 0, 0]), vec![2, 2, 2]),
)] fn shape_test(arr: Array<f64>, expected: Vec<usize>) { assert_eq!(arr.get_shape(), expected); }

#[rstest(
arr, index, expected,
case(Array::new_masked(gen_e(4), gen_s(2), gen_m(4)), vec![0, 1], 2.),
case(Array::new_masked(gen_e(8), gen_s(3), gen_m(8)), vec![0, 1, 1], 4.),
case(Array::new_masked(gen_e(16), gen_s(4), gen_m(16)), vec![0, 1, 0, 1], 6.),
case(Array::new_masked(gen_e(32), gen_s(5), gen_m(32)), vec![0, 1, 0, 1, 0], 11.),
)] fn get_element_at_test(arr: Array<f64>, index: Vec<usize>, expected: f64) { assert!(arr.at_v(index).approx_eq(expected)); }
fn gen_e(count: usize) -> Vec<f64> { (0..count).map(|i| (i + 1).to_float()).collect() }
fn gen_s(count: usize) -> Vec<usize> { (0..count).map(|_| 2).collect() }
fn gen_m(count: usize) -> Vec<usize> { (0..count).map(|_| 0).collect() }

#[rstest(
arr, axes, expected,
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![0, 1, 2]), array![[[1.0000, 2.0000], [3.0000, 4.0000]], [[5.0000, 6.0000], [7.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![0, 2, 1]), array![[[1.0000, 3.0000], [2.0000, 4.0000]], [[5.0000, 7.0000], [6.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![1, 0, 2]), array![[[1.0000, 2.0000], [5.0000, 6.0000]], [[3.0000, 4.0000], [7.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![1, 2, 0]), array![[[1.0000, 5.0000], [2.0000, 6.0000]], [[3.0000, 7.0000], [4.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![2, 0, 1]), array![[[1.0000, 3.0000], [5.0000, 7.0000]], [[2.0000, 4.0000], [6.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], Some(vec![2, 1, 0]), array![[[1.0000, 5.0000], [3.0000, 7.0000]], [[2.0000, 6.0000], [4.0000, 8.0000]]]),
case(array![[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]], None, array![[[1.0000, 5.0000], [3.0000, 7.0000]], [[2.0000, 6.0000], [4.0000, 8.0000]]]),
)] fn transpose_test(arr: Array<f64>, axes: Option<Vec<usize>>, expected: Array<f64>) {
    assert_eq!(arr.transpose(axes), expected)
}
