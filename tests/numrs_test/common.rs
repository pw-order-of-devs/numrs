use numrs::prelude::*;

pub(crate) fn test_runner<S: ArrayType>(
    arg1: &impl ArrayImpl<S>,
    arg2: &impl ArrayImpl<S>,
) {
    assert_eq!(arg1.get_shape(), arg2.get_shape(), "Shapes don't match");
    assert_eq!(arg1.get_mask(), arg2.get_mask(), "Masks don't match");
    let (elems_1, elems_2) = (arg1.filter_by_mask().get_elements(), arg2.filter_by_mask().get_elements());
    (0..elems_1.len()).for_each(|i| assert_eq!(elems_1[i], elems_2[i], "{} != {}", arg1, arg2));
}

pub(crate) fn test_runner_gen<S1: ArrayType, S2: ArrayType>(
    arg1: &impl ArrayImpl<S1>,
    arg2: &impl ArrayImpl<S2>,
) {
    let (elems_1, elems_2) = (arg1.get_elements(), arg2.get_elements());
    (0..elems_1.len()).for_each(|i| assert_eq!(elems_1[i], elems_2[i], "{} != {}", arg1, arg2));
}

pub(crate) fn arr_fv<S: ArrayType>(val: S, shape: Vec<usize>) -> Array<S> {
    Array::new((0..shape.mult()).map(|_| val).collect(), shape)
}
