pub mod prelude;

#[cfg(not(tarpaulin_include))]
pub mod base {
    pub mod prelude;

    pub mod array;
    pub mod array_type;
    pub mod axis;
    pub mod broadcast;
    pub mod display;
    pub mod hist;
    pub mod iter;
    pub mod limits;
    pub mod ops;
    pub mod transpose;
    pub mod types;
}

pub mod arrays {
    pub mod array;
    #[macro_use] pub mod macros;
}

pub mod ext {
    pub mod primitives;
    pub mod vec;
}
