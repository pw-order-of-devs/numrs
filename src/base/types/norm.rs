use crate::base::array_type::type_def::ArrayType;

#[derive(Debug, Clone, PartialOrd, PartialEq, Eq)]
pub enum OrdType { Numeric, Fro, Nuc, }

#[derive(Debug, Clone, PartialOrd, PartialEq, Eq)]
pub struct Ord<S: ArrayType> {
    pub(crate) value: Option<S>,
    pub(crate) value_type: OrdType,
}

impl <S: ArrayType>Ord<S> {

    pub fn new(value: S) -> Self {
        Self::numeric(value)
    }

    pub fn newf(value: f64) -> Self {
        Self::numeric(S::from_float(value))
    }

    pub fn numeric(value: S) -> Self {
        Ord { value: Some(value), value_type: OrdType::Numeric, }
    }

    pub fn frobenius() -> Self {
        Ord { value: None, value_type: OrdType::Fro, }
    }

    pub fn nuclear() -> Self {
        Ord { value: None, value_type: OrdType::Nuc, }
    }
}
