use crate::prelude::*;

pub trait ArrayLimits<S: ArrayType> {
    fn filter_by_limits(&self, lim: (S, S), inclusive: (bool, bool)) -> Self;
    fn filter_by_limits_vec(&self, lim: (S, S), inclusive: (bool, bool)) -> Vec<ArrayElem<S>>;

    fn filter_by_limits_opt(&self, lim: (Option<S>, Option<S>), inclusive: (bool, bool)) -> Self;
    fn filter_by_limits_vec_opt(&self, lim: (Option<S>, Option<S>), inclusive: (bool, bool)) -> Vec<ArrayElem<S>>;
}

impl <S: ArrayType>ArrayLimits<S> for Array<S> {

    fn filter_by_limits(&self, lim: (S, S), inclusive: (bool, bool)) -> Array<S> {
        self.filter(|i| if inclusive.0 { i.value >= lim.0 } else { i.value > lim.0 })
            .filter(|i| if inclusive.1 { i.value <= lim.1 } else { i.value < lim.1 })
    }

    fn filter_by_limits_vec(&self, lim: (S, S), inclusive: (bool, bool)) -> Vec<ArrayElem<S>> {
        self.filter_by_limits(lim, inclusive).get_elements()
    }

    fn filter_by_limits_opt(&self, lim: (Option<S>, Option<S>), inclusive: (bool, bool)) -> Self {
        self.filter(|i| if let Some(l) = lim.0 { if inclusive.0 { i.value >= l } else { i.value > l } } else { true })
            .filter(|i| if let Some(l) = lim.1 { if inclusive.1 { i.value <= l } else { i.value < l } } else { true } )
    }

    fn filter_by_limits_vec_opt(&self, lim: (Option<S>, Option<S>), inclusive: (bool, bool)) -> Vec<ArrayElem<S>> {
        self.filter_by_limits_opt(lim, inclusive).get_elements()
    }
}
