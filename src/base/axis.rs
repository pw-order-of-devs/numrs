use crate::prelude::*;

pub trait ArrayAxis<S: ArrayType> where Self: Sized {
    fn get_by_axis(&self, axis: usize) -> Self;
    fn take(&self, indices: &[usize], axis: usize) -> Self;
    fn sort(&self, axis: Option<usize>) -> Self;
    fn any<F: Clone + FnMut(&ArrayElem<S>) -> bool>(&self, f: F, axis: Option<usize>, keepdims: Option<bool>) -> Array<bool>;
    fn all<F: Clone + FnMut(&ArrayElem<S>) -> bool>(&self, f: F, axis: Option<usize>, keepdims: Option<bool>) -> Array<bool>;
    fn max(&self, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn min(&self, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn moveaxis(&self, source: Vec<isize>, destination: Vec<isize>) -> Self;
    fn argsort(&self, axis: Option<usize>) -> Self;
    fn sum(&self, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn std(&self, axis: Option<usize>, ddof: usize, keepdims: Option<bool>) -> Self;
    fn var(&self, axis: Option<usize>, ddof: usize, keepdims: Option<bool>) -> Self;
    fn count(&self, axis: Option<usize>, should_filter: Option<bool>) -> Array<usize>;
    fn median(&self, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn mean(&self, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn diff(&self, axis: Option<usize>) -> Self;
    fn cumsum(&self, axis: Option<usize>) -> Self;
    fn concatenate(&self, arrs: &[Self], axis: Option<usize>) -> Self;
    fn norm(&self, ord: Option<Ord<S>>, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn validate_axis(&self, axis: Option<usize>) -> usize;
    fn squeeze(&self, axis: Option<usize>) -> Self;
    fn with_keepdims(self, out_dims: Vec<usize>, axis: Option<usize>, keepdims: Option<bool>) -> Self;
    fn by_axis_vars(&self, axis: usize) -> (Self, usize, usize);
    fn by_axis_vars_opt(&self, axis: Option<usize>) -> ((Self, usize, usize), usize);
    fn by_axis_new_shape(&self, axis: usize) -> Vec<usize>;
}

impl <S: ArrayType>ArrayAxis<S> for Array<S> {

    fn get_by_axis(&self, axis: usize) -> Array<S> {
        if self.ndim() == 1 { self.clone() }
        else if vec![0, self.ndim() - 1].contains(&axis) {
            let arr_len = self.get_shape()[axis];
            let arrs_count = self.len() / arr_len;
            let res = (0..arrs_count).flat_map(|i|
                (0..arr_len).map(|j|
                    self.clone().get_elements()[if axis == 0 { j * arrs_count + i } else { j + i * arr_len }]
                ).collect::<Vec<ArrayElem<S>>>()
            ).collect();
            let mut res_shape = self.get_shape();
            if axis == 0 { res_shape.reverse(); }
            Array::new_elems(res, res_shape)
        } else {
            let mut shape = self.get_shape();
            shape.remove(axis - 1);
            let ln = shape.mult();
            let res = (0..self.get_shape()[axis - 1])
                .map(|i| self.subarray(i * ln, (i + 1) * ln).update_shape(shape.clone()))
                .collect::<Vec<Self>>()
                .into_iter()
                .flat_map(|a| Array::get_by_axis(&a, axis - 1).get_elements())
                .collect();
            let mut res_shape = self.get_shape();
            let rem = res_shape.remove(axis);
            res_shape.insert(self.ndim() - 1, rem);
            Array::new_elems(res, res_shape)
        }
    }

    fn take(&self, indices: &[usize], axis: usize) -> Array<S> {
        let arr_len = self.get_shape()[axis];
        let arrs_count = self.len() / arr_len;
        let mut items = vec![];
        for x in (0 .. arrs_count).collect::<Vec<usize>>() {
            let row = self.subarray(x * arr_len, (x + 1) * arr_len);
            let mut partial_e = vec![];
            indices.iter().for_each(|i| {
                partial_e.push(row.at(*i));
            });
            items.extend_from_slice(&partial_e);
        }
        let mut new_shape = self.get_shape();
        new_shape[axis] = indices.len();
        Array::new_elems(items, new_shape)
    }

    fn sort(&self, axis: Option<usize>) -> Array<S> {
        if self.ndim() == 1 {
            let mut elems = self.get_elements();
            elems.sort_by(|a, b| a.ord(b));
            self.update_elements(elems)
        } else {
            let axis = axis.unwrap_or(self.ndim() - 1);
            let (by_axis, arr_len, arrs_count)  = self.by_axis_vars(axis);
            let arrs = (0..arrs_count).map(|i| by_axis.subarray(i * arr_len, (i + 1) * arr_len).sort(None)).collect::<Vec<Self>>();
            Array::new(build_by_axis(arrs, self.get_shape(), self.ndim(), axis), self.get_shape())
        }
    }

    fn any<F: Clone + FnMut(&ArrayElem<S>) -> bool>(&self, mut f: F, axis: Option<usize>, keepdims: Option<bool>) -> Array<bool> {
        match axis {
            None => Array::new_flat(vec![self.filter_by_mask().get_elements().into_iter().any(|s| f(&s))]),
            Some(ax) => any_axis(self, f, ax, keepdims)
        }
    }

    fn all<F: Clone + FnMut(&ArrayElem<S>) -> bool>(&self, mut f: F, axis: Option<usize>, keepdims: Option<bool>) -> Array<bool> {
        match axis {
            None => Array::new_flat(vec![self.filter_by_mask().get_elements().into_iter().all(|s| f(&s))]),
            Some(ax) => all_axis(self, f, ax, keepdims)
        }
    }

    fn max(&self, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None => Array::new_elems_flat(vec![self.sort(None).at(self.len() - 1)]),
            Some(ax) => max_axis(self, ax, keepdims)
        }
    }

    fn min(&self, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None => Array::new_elems_flat(vec![self.sort(None).at(0)]),
            Some(ax) => min_axis(self, ax, keepdims)
        }
    }

    fn moveaxis(&self, source: Vec<isize>, destination: Vec<isize>) -> Array<S> {
        assert_eq!(source.len(), destination.len(), "source and destination must have the same length");
        let source = normalize_axis(self, source);
        let destination = normalize_axis(self, destination);
        let mut order = (0 .. self.ndim()).filter(|f| !source.contains(f)).collect::<Vec<usize>>();
        let mut zipped = destination.into_iter()
            .zip(source.iter())
            .map(|(a, b)| (a, *b))
            .collect::<Vec<(usize, usize)>>();
        zipped.sort_unstable();
        for (d, s) in zipped {
            if d >= order.len() { order.push(s) }
            else { order.insert(d, s) }
        }
        self.transpose(Some(order))
    }

    fn argsort(&self, axis: Option<usize>) -> Array<S> {
        if self.ndim() == 1 {
            let mut res = vec![];
            self.sort(None).map(|v| {
                let pos = S::from_usize(self.get_elements().into_iter().enumerate().position(|(i, f)|
                    f == *v && res.clone().contains(&S::from_usize(i)).not()
                ).unwrap());
                res.push(pos);
                pos.to_elem()
            })
        } else {
            let axis = axis.unwrap_or(self.ndim() - 1);
            let (by_axis, arr_len, arrs_count)  = self.by_axis_vars(axis);
            let arrs = (0..arrs_count).map(|i| by_axis
                .subarray(i * arr_len, (i + 1) * arr_len).argsort(None)
            ).collect::<Vec<Self>>();
            Array::new(build_by_axis(arrs, self.get_shape(), self.ndim(), axis), self.get_shape())
        }
    }

    fn sum(&self, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None => Array::new_elems_flat(vec![self.clone().fold(ArrayElem::zero(), |a, b| *a + *b)]),
            Some(ax) => sum_axis(self, ax, keepdims)
        }
    }

    fn std(&self, axis: Option<usize>, ddof: usize, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None => {
                let mean = self.mean(None, None).single();
                let len = self.filter_by_mask().len();
                if ddof > len { Array::new_elems_flat(vec![ArrayElem::nan()]) }
                else {
                    let res = self.clone()
                        .map(|i| (*i - mean).powf(S::two()))
                        .sum(None, None).single() / ArrayElem::<S>::from_usize(len - ddof);
                    Array::new_elems_flat(vec![res.sqrt()])
                }
            },
            Some(ax) => std_axis(self, ax, ddof, keepdims)
        }
    }

    fn var(&self, axis: Option<usize>, ddof: usize, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None => {
                let mean = self.mean(None, None).single();
                let len = self.filter_by_mask().len();
                if mean.mask == 1 || ddof > len { Array::new_elems_flat(vec![ArrayElem::nan()]) }
                else {
                    let len = self.filter_by_mask().len();
                    let res = self.clone()
                        .map(|i| (*i - mean).powf(S::from_usize(2)))
                        .sum(None, None).single() / ArrayElem::<S>::from_usize(len - ddof);
                    Array::new_elems_flat(vec![res])
                }
            },
            Some(ax) => var_axis(self, ax, ddof, keepdims)
        }
    }

    fn count(&self, axis: Option<usize>, should_filter: Option<bool>) -> Array<usize> {
        match (axis, should_filter) {
            (None, None) | (None, Some(false)) => Array::new_flat(vec![self.len()]),
            (None, Some(true)) => Array::new_flat(vec![self.values_len()]),
            (Some(ax), _) => count_axis(self, ax, should_filter.unwrap_or(false)),
        }
    }

    fn median(&self, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        if self.is_empty() || self.len() == 1 { return self.ravel() }
        match axis {
            None => {
                let arr: Vec<ArrayElem<S>> = self.ravel().sort(None).get_elements();
                if arr.len() % 2 == 1 { Array::new_elems_flat(vec![arr[(arr.len() - 1) / 2]]) }
                else { Array::new_elems_flat(vec![(arr[arr.len() / 2] + arr[arr.len() / 2 - 1]) / ArrayElem::<S>::two()]) }
            },
            Some(ax) => median_axis(self, ax, keepdims)
        }
    }

    fn mean(&self, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        match axis {
            None =>
                if self.get_elements().is_empty() { Array::new_elems_flat(vec![ArrayElem::nan()]) }
                else {
                    let filtered = self.filter_by_mask();
                    let len = filtered.len();
                    if filtered.all(|i| i.mask() == 1, None, None).single().to_bool() || len == 0 {
                        Array::new_elems_flat(vec![ArrayElem::new_masked(S::nan(), 1)])
                    }
                    else { Array::new_elems_flat(vec![self.sum(None, None).single() / ArrayElem::<S>::from_usize(len)]) }
                },
            Some(ax) => mean_axis(self, ax, keepdims)
        }
    }

    fn diff(&self, axis: Option<usize>) -> Array<S> {
        match self.ndim() {
            1 => Array::new_flat((1..self.len()).map(|i| self.at(i) - self.at(i - 1)).collect()),
            _ => diff_axis(self, axis.unwrap_or(self.ndim() - 1))
        }
    }

    fn cumsum(&self, axis: Option<usize>) -> Array<S> {
        let arr = if axis.is_none() { self.ravel() } else { self.clone() };
        if arr.ndim() == 1 {
            arr.update_elements((1..arr.len() + 1).map(|i| arr.subarray(0, i).sum(None, None).single()).collect())
        } else {
            let axis = axis.unwrap_or(arr.ndim() - 1);
            let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
            let arrs = (0..arrs_count).map(|i| by_axis.subarray(i * arr_len, (i + 1) * arr_len).cumsum(None)).collect::<Vec<Self>>();
            Array::new(build_by_axis(arrs, arr.get_shape(), arr.ndim(), axis), arr.get_shape())
        }
    }

    fn concatenate(&self, arrs: &[Self], axis: Option<usize>) -> Array<S> {
        if self.ndim() == 1 && arrs.iter().all(|a| a.ndim() == 1) {
            return _concatenate_1d(self, arrs)
        }
        for arr in arrs { assert_eq!(self.get_shape(), arr.get_shape(), "All shapes must match"); }
        if arrs.is_empty() { return self.clone() }
        match axis {
            None => {
                let mut elements = self.get_elements();
                arrs.iter().for_each(|i| elements.extend_from_slice(&i.get_elements()));
                Array::new_elems_flat(elements)
            },
            Some(a) => {
                let mut new_shape = self.get_shape();
                new_shape[a] *= arrs.len() + 1;
                let mut len = self.get_shape();
                for _ in 0..a { len.remove(0); }
                let len = len.mult();
                let times = self.len() / len;
                let mut result = vec![];
                for x in 0..times {
                    result.extend_from_slice(&self.subarray(x * len, (x + 1) * len).get_elements());
                    arrs.iter().for_each(|arr| result.extend_from_slice(&arr.subarray(x * len, (x + 1) * len).get_elements()))
                }
                Array::new_elems(result, new_shape)
            }
        }
    }

    fn norm(&self, ord: Option<Ord<S>>, axis: Option<usize>, keepdims: Option<bool>) -> Self {
        if axis.is_none() && (ord.is_none()
            || (ord == Some(Ord::frobenius()) && self.ndim() == 2)
            || (ord == Some(Ord::numeric(S::two())) && self.ndim() == 1)) {
            let ravel = self.ravel();
            return ravel.clone().dot(ravel).sqrt().with_keepdims(self.get_shape(), axis, keepdims);
        };
        let axis = if let Some(a) = axis { vec![a] } else { (0 .. self.ndim()).collect() };
        if axis.len() == 1 {
            let axis = axis[0];
            if ord == Some(Ord::newf(f64::inf())) { self.abs().max(Some(axis), keepdims) }
            else if ord == Some(Ord::newf(f64::neg_inf())) { self.abs().min(Some(axis), keepdims) }
            else if ord == Some(Ord::newf(0.)) {
                self.map(|i| if *i != 0. { ArrayElem::one() } else { ArrayElem::zero() } ).sum(Some(axis), keepdims)
            } else if ord == Some(Ord::newf(1.)) {
                self.abs().sum(Some(axis), keepdims)
            } else if ord.is_none() || ord == Some(Ord::newf(2.)) {
                self.powf(2.).sum(Some(axis), keepdims).sqrt()
            } else if ord == Some(Ord::frobenius()) || ord == Some(Ord::nuclear()) {
                panic!("Invalid norm order '{:?}' for vectors", ord.unwrap().value_type)
            } else {
                let ord = if let Some(o) = ord { o.value.unwrap().to_float() } else { 1. };
                self.abs().powf(ord).sum(Some(axis), keepdims).powf(1. / ord)
            }
        } else if axis.len() == 2 {
            panic!("Not yet supported")
        } else { panic!("Improper number of dimensions to norm.") }
    }

    fn validate_axis(&self, axis: Option<usize>) -> usize {
        if let Some(a) = axis {
            assert!(a < self.ndim(), "Axis {} is out of bounds for matrix of dimension {}", a, self.ndim());
            a
        } else { 0_usize }
    }

    fn squeeze(&self, axis: Option<usize>) -> Array<S> {
        if let Some(ax) = axis {
            if self.get_shape()[ax] == 1 {
                let mut new_shape = self.get_shape();
                new_shape.remove(ax);
                self.update_shape(new_shape)
            } else {
                panic!("Cannot select an axis with size not equal to one")
            }
        }
        else { self.update_shape(self.get_shape().into_iter().filter(|i| *i != 1).collect()) }
    }

    fn with_keepdims(self, out_dims: Vec<usize>, axis: Option<usize>, keepdims: Option<bool>) -> Array<S> {
        if if let Some(kd) = keepdims { kd } else { false } {
            let mut out_dims = out_dims;
            for a in &[axis.unwrap_or(0)] { out_dims[*a] = 1 }
            self.update_shape(out_dims)
        } else { self }
    }

    fn by_axis_vars(&self, axis: usize) -> (Self, usize, usize) {
        let by_axis = self.clone().get_by_axis(axis);
        let arr_len = *by_axis.get_shape().last().unwrap();
        let arrs_count = by_axis.len() / arr_len;
        (by_axis, arr_len, arrs_count)
    }

    fn by_axis_vars_opt(&self, axis: Option<usize>) -> ((Self, usize, usize), usize) {
        match axis {
            None => (self.ravel().by_axis_vars(0), 0),
            Some(ax) => (self.by_axis_vars(ax), ax),
        }
    }

    fn by_axis_new_shape(&self, axis: usize) -> Vec<usize> {
        if self.ndim() == 1 { vec![1] } else {
            let mut ns = self.get_shape();
            ns.remove(self.ndim() - 1);
            if axis == 0 { ns.reverse(); }
            ns
        }
    }
}

fn build_by_axis<S: ArrayType>(arrs: Vec<Array<S>>, shape: Vec<usize>, ndim: usize, axis: usize) -> Vec<S> {
    if axis == ndim - 1 { arrs.into_iter().flat_map(|i| i.get_elements()).collect() }
    else {
        let joined = arrs.into_iter().flatten().collect::<Vec<S>>();
        let subarrs = if axis == 0 { 1 } else { shape[0] };
        let subarr_len = joined.len() / subarrs;
        let partial = (0..subarrs)
            .map(|i| joined[(i * subarr_len) .. (i + 1) * subarr_len].to_vec())
            .collect::<Vec<Vec<S>>>();
        let mut res = vec![];
        partial.into_iter().for_each(|p|
            (0..shape[axis]).for_each(|i|
                res.push(p.clone().into_iter()
                    .enumerate().filter(|(f, _)| *f % shape[axis] == i)
                    .map(|t| t.1)
                    .collect::<Vec<S>>())
            )
        );
        res.into_iter().flatten().collect()
    }
}

fn any_axis<S: ArrayType, F: Clone + FnMut(&ArrayElem<S>) -> bool>(arr: &Array<S>, f: F, axis: usize, keepdims: Option<bool>) -> Array<bool> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).any(f.clone(), None, keepdims).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn all_axis<S: ArrayType, F: Clone + FnMut(&ArrayElem<S>) -> bool>(arr: &Array<S>, f: F, axis: usize, keepdims: Option<bool>) -> Array<bool> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).all(f.clone(), None, keepdims).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)

}

fn max_axis<S: ArrayType>(arr: &Array<S>, axis: usize, keepdims: Option<bool>) -> Array<S> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).max(None, keepdims).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)

}

fn min_axis<S: ArrayType>(arr: &Array<S>, axis: usize, keepdims: Option<bool>) -> Array<S> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).min(None, keepdims).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis))
}

fn sum_axis<S: ArrayType>(arr: &Array<S>, axis: usize, keepdims: Option<bool>) -> Array<S> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).sum(None, None).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn std_axis<S: ArrayType>(arr: &Array<S>, axis: usize, ddof: usize, keepdims: Option<bool>) -> Array<S> {
    _validate_axis_ddof(arr, axis, ddof);
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).std(None, ddof, keepdims).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn var_axis<S: ArrayType>(arr: &Array<S>, axis: usize, ddof: usize, keepdims: Option<bool>) -> Array<S> {
    _validate_axis_ddof(arr, axis, ddof);
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).var(None, ddof, None).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn _validate_axis_ddof<S: ArrayType>(arr: &Array<S>, axis: usize, ddof: usize) {
    assert!(axis < arr.ndim(), "Axis not supported");
    assert!(
        ddof > 0 || arr.count(Some(axis), None).any(|i| i.value() >= ddof, None, None).single().to_bool(),
        "DDOF cannot be lower than zero or greater than axis length"
    );
}

fn count_axis<S: ArrayType>(arr: &Array<S>, axis: usize, should_filter: bool) -> Array<usize> {
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i| {
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).count(None, Some(should_filter)).single()
    }).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis))
}

fn mean_axis<S: ArrayType>(arr: &Array<S>, axis: usize, keepdims: Option<bool>) -> Array<S> {
    let (by_axis, arr_len, arrs_count) = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).mean(None, None).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn median_axis<S: ArrayType>(arr: &Array<S>, axis: usize, keepdims: Option<bool>) -> Array<S> {
    let (by_axis, arr_len, arrs_count) = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).median(None, None).single()
    ).collect();
    Array::new_elems(arrs, by_axis.by_axis_new_shape(axis)).with_keepdims(arr.get_shape(), Some(axis), keepdims)
}

fn diff_axis<S: ArrayType>(arr: &Array<S>, axis: usize) -> Array<S> {
    assert!(axis < arr.ndim(), "Axis not supported");
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    let arrs = (0..arrs_count).map(|i|
        by_axis.subarray(i * arr_len, (i + 1) * arr_len).diff(None).single()
    ).collect();
    let mut new_shape = by_axis.get_shape();
    new_shape[axis] = 1;
    Array::new_elems(arrs, new_shape)
}

fn normalize_axis<S: ArrayType>(arr: &Array<S>, axes: Vec<isize>) -> Vec<usize> {
    let indim = arr.ndim() as isize;
    axes.clone().into_iter()
        .for_each(|f| assert!(f >= 0 && f < indim || f < 0 && f >= -(indim), "Axis {} is out  of bounds of array", f));
    axes.into_iter()
        .map(|f| if f >= 0 && f < indim { f } else { indim + (f + 1) })
        .map(|f| f as usize)
        .collect::<Vec<usize>>()
}

fn _concatenate_1d<S: ArrayType>(arr: &Array<S>, arrs: &[Array<S>]) -> Array<S> {
    let mut head = arr.get_elements();
    let tail = Array::new_elems_flat(arrs.iter()
        .flat_map(|a| a.get_elements())
        .collect());
    head.extend_from_slice(&tail.get_elements());
    Array::new_elems_flat(head)
}
