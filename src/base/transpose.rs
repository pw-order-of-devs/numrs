use crate::prelude::*;

pub trait ArrayTranspose<S: ArrayType> {
    fn transpose(&self, axes: Option<Vec<usize>>) -> Self;
}

impl <S: ArrayType>ArrayTranspose<S> for Array<S> {

    fn transpose(&self, axes: Option<Vec<usize>>) -> Array<S> {
        if self.ndim() == 1 { return self.clone(); }
        Helper::not_supported_clause(self, axes.clone());
        let axes = Helper::validate_axes(self, axes);
        if axes == (0..self.ndim()).collect::<Vec<usize>>() { self.clone() }
        else {
            let axes = Helper::build_new_axes(self, axes);
            let mut new_shape = (0..self.ndim()).map(|i| self.get_shape()[axes[i]]).collect::<Vec<usize>>();
            (0..Helper::calc_offset(self, &axes)).for_each(|_| new_shape.move_elem(0, self.ndim() - 1));
            Array::new(Helper::build_new_elements(self, new_shape.clone(), axes), new_shape)
        }
    }
}

struct Helper<S: ArrayType> { _s: std::marker::PhantomData<S>, }
impl <S: ArrayType> Helper<S> {
    fn not_supported_clause(arr: &Array<S>, axes: Option<Vec<usize>>) {
        if arr.ndim() < 4 || axes.is_none() {} else if let Some(ax) = axes {
            let ax_s = (0..arr.ndim()).collect::<Vec<usize>>();
            if ax == ax_s || ax == ax_s.into_iter().rev().collect::<Vec<usize>>() {} else { panic!("Not yet supported.") }
        }
    }

    fn validate_axes(arr: &Array<S>, axes: Option<Vec<usize>>) -> Vec<usize> {
        let axes =
            if let Some(ax) = axes { ax } else { (0..arr.ndim()).rev().collect() };
        let mut unique = axes.clone();
        unique.sort_unstable();
        unique.dedup();
        assert_eq!(unique.len(), axes.len(), "Repeated axis in transpose");
        assert_eq!(arr.ndim(), axes.len(), "Axes don't match");
        axes
    }

    fn build_new_axes(arr: &Array<S>, axes: Vec<usize>) -> Vec<usize> {
        let has_sub_f = (0..2)
            .map(|i| ((i..arr.ndim() - 1 + i).collect::<Vec<usize>>()))
            .map(|i| Self::is_sub(&axes, &i))
            .filter(|i| i.0 && i.1 >= 0)
            .collect::<Vec<(bool, isize)>>();
        let has_sub = match has_sub_f.len() {
            1 => Some(has_sub_f[0]),
            _ => None,
        };
        match has_sub {
            None => axes,
            Some(v) => {
                let mut axs = axes;
                (0..arr.ndim() - 2 + v.1 as usize).for_each(|_| {
                    let rem = axs.remove(0);
                    axs.push(rem);
                });
                axs
            }
        }
    }

    fn build_new_elements(arr: &Array<S>, shape: Vec<usize>, axes: Vec<usize>) -> Vec<S> {
        (0..shape.mult()).map(|i| {
            let (mut s, mut idx) = (0, i);
            let mut ids = vec![];
            shape.clone().into_iter().enumerate().for_each(|(ii, _)| {
                s = arr.len() / shape[..ii + 1].to_vec().mult();
                let curr_idx = f64::ceil((idx / s) as f64) as usize;
                idx -= s * curr_idx;
                ids.push(curr_idx);
            });
            arr.at_v((0..arr.ndim()).map(|i| ids[axes[i]]).collect())
        }).collect()
    }

    fn calc_offset(_: &Array<S>, vec: &[usize]) -> usize {
        let (lens, mut strs, mut res) = ((0..vec.len() - 2).map(|i| i + 2).collect::<Vec<usize>>(), vec![], 0);
        for l in lens { (0..vec.len() - l + 1).for_each(|i| strs.push((0..l).map(|ii| ii + i).collect::<Vec<usize>>())) }
        for s in strs {
            let (is, idx) = Self::is_sub(vec, &s);
            if is { res += 1 + idx as usize }
        }
        res
    }

    fn is_sub(mut vec: &[usize], sub: &[usize]) -> (bool, isize) {
        if sub.is_empty() { return (true, -1); }
        let mut idx = 0;
        while !vec.is_empty() {
            if vec.starts_with(sub) { return (true, idx); }
            idx += 1;
            vec = &vec[1..];
        }
        (false, -1)
    }
}
