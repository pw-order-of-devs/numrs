use crate::prelude::{Array, ArrayBase, ArrayElem, ArrayType};

pub trait At<S: ArrayType> {
    fn at(&self, idx: usize) -> ArrayElem<S>;
    fn at_v(&self, idx: Vec<usize>) -> ArrayElem<S>;
}

impl <S: ArrayType>At<S> for Array<S> {

    fn at(&self, idx: usize) -> ArrayElem<S> {
        self.by_idx_flat(idx)
    }

    fn at_v(&self, idx: Vec<usize>) -> ArrayElem<S> {
        self.get_element_at(idx)
    }
}
