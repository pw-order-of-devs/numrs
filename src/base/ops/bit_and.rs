use std::ops::BitAnd;

use crate::prelude::*;

impl BitAnd<ArrayElem<bool>> for bool {
    type Output = bool;
    fn bitand(self, other: ArrayElem<bool>) -> Self::Output {
        self & other.value
    }
}

impl BitAnd<ArrayElem<bool>> for ArrayElem<bool> {
    type Output = ArrayElem<bool>;
    fn bitand(self, other: ArrayElem<bool>) -> Self::Output {
        (self.value & other.value).to_elem()
    }
}
