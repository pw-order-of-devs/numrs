use std::ops::Mul;

use crate::prelude::*;

pub(crate) fn mul<S1: ArrayType, S2: ArrayType>(s1: S1, s2: S2) -> S1 {
    S1::from_float(s1.to_float() * s2.to_float())
}

impl <S1: ArrayType, S2: ArrayType>Mul<ArrayElem<S2>> for ArrayElem<S1> {
    type Output = ArrayElem<S1>;
    fn mul(self, other: ArrayElem<S2>) -> Self::Output {
        (S1::from_float(self.value.to_float() * other.value.to_float())).to_elem()
    }
}

// Mul - scalar

impl <S1: ArrayType, S2: ArrayType>Mul<S2> for Array<S1> {
    type Output = Array<S1>;
    fn mul(self, other: S2) -> Self::Output {
        self.map(|s| mul(s.value, other).to_elem())
    }
}

impl <S1: ArrayType, S2: ArrayType>Mul<ArrayElem<S2>> for Array<S1> {
    type Output = Array<S1>;
    fn mul(self, other: ArrayElem<S2>) -> Self::Output {
        self.map(|s| mul(s.value, other.value).to_elem())
    }
}

impl <S: ArrayType>Mul<Array<S>> for f64 {
    type Output = Array<S>;
    fn mul(self, other: Array<S>) -> Self::Output {
        other.map(|s| mul(S::from_float(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Mul<Array<S>> for i32 {
    type Output = Array<S>;
    fn mul(self, other: Array<S>) -> Self::Output {
        other.map(|s| mul(S::from_i32(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Mul<Array<S>> for usize {
    type Output = Array<S>;
    fn mul(self, other: Array<S>) -> Self::Output {
        other.map(|s| mul(S::from_usize(self), s.value).to_elem())
    }
}

// Mul

impl <S1: ArrayType, S2: ArrayType>Mul<Array<S2>> for Array<S1> {
    type Output = Array<S1>;

    fn mul(self, other: Array<S2>) -> Self::Output {
        if other.len() == 1 { return self * other.single(); }
        let (arr1, arr2) = Self::broadcast(&self, &other);
        arr1.map_enumerate(|i, s|
            *s * arr2.get_elements()[i]
        )
    }
}
