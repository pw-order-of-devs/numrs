use std::ops::*;

use crate::prelude::*;

pub mod at;
pub mod add;
pub mod sub;
pub mod mul;
pub mod div;
pub mod dot;
pub mod matmul;
pub mod not;
pub mod bit_and;
pub mod partial_eq;

trait With where Self: Sized { fn with(&self, other: Self) -> Self; }
impl With for Vec<usize> {
    fn with(&self, other: Self) -> Self {
        self.iter().enumerate().map(|(i, _)| if self[i] == 0 && other[i] == 0 { 0 } else { 1 }).collect::<Self>()
    }
}

impl <S: ArrayType>ArrayOps<S> for Array<S> {}
pub trait ArrayOps<S: ArrayType>: Add + Sub + Mul + Div + PartialEq
    where Self: ArrayBase<S> {

    fn powf(&self, e: f64) -> Self { self.map(|s| s.powf(S::from_float(e))) }
    fn sqrt(&self) -> Self { self.map(|s| s.sqrt()) }
    fn abs(&self) -> Self { self.map(|s| s.abs()) }
    fn exp(&self) -> Self { self.map(|s| s.exp()) }
    fn ln(&self) -> Self { self.map(|s| s.ln()) }
}

fn sum_product<S1: ArrayType, S2: ArrayType>(arr1: Array<S1>, arr2: Array<S2>) -> ArrayElem<S1> {
    (0..arr1.len())
        .map(|i| arr1.at(i) * arr2.at(i))
        .fold(ArrayElem::<S1>::zero(), |a, b| a + b)
}
