use std::ops::Div;

use crate::prelude::*;

pub(crate) fn div<S1: ArrayType, S2: ArrayType>(s1: S1, s2: S2) -> S1 {
    S1::from_float(s1.to_float() / s2.to_float())
}

impl <S1: ArrayType, S2: ArrayType>Div<ArrayElem<S2>> for ArrayElem<S1> {
    type Output = ArrayElem<S1>;
    fn div(self, other: ArrayElem<S2>) -> Self::Output {
        (S1::from_float(self.value.to_float() / other.value.to_float())).to_elem()
    }
}

// Div - scalar

impl <S1: ArrayType, S2: ArrayType>Div<S2> for Array<S1> {
    type Output = Array<S1>;
    fn div(self, other: S2) -> Self::Output {
        self.map(|s| div(s.value, other).to_elem())
    }
}

impl <S1: ArrayType, S2: ArrayType>Div<ArrayElem<S2>> for Array<S1> {
    type Output = Array<S1>;
    fn div(self, other: ArrayElem<S2>) -> Self::Output {
        self.map(|s| div(s.value, other.value).to_elem())
    }
}

impl <S: ArrayType>Div<Array<S>> for f64 {
    type Output = Array<S>;
    fn div(self, other: Array<S>) -> Self::Output {
        other.map(|s| div(S::from_float(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Div<Array<S>> for i32 {
    type Output = Array<S>;
    fn div(self, other: Array<S>) -> Self::Output {
        other.map(|s| div(S::from_i32(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Div<Array<S>> for usize {
    type Output = Array<S>;
    fn div(self, other: Array<S>) -> Self::Output {
        other.map(|s| div(S::from_usize(self), s.value).to_elem())
    }
}

// Div

impl <S1: ArrayType, S2: ArrayType>Div<Array<S2>> for Array<S1> {
    type Output = Array<S1>;

    fn div(self, other: Array<S2>) -> Self::Output {
        if other.len() == 1 { return self / other.single(); }
        let (arr1, arr2) = Self::broadcast(&self, &other);
        arr1.map_enumerate(|i, s|
            *s / arr2.get_elements()[i]
        )
    }
}
