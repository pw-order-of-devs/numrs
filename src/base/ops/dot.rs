use crate::base::ops::sum_product;
use crate::prelude::*;

pub trait Dot<S1: ArrayType, S2: ArrayType> {
    fn dot(&self, other: Array<S2>) -> Self;
}

impl <S1: ArrayType, S2: ArrayType>Dot<S1, S2> for Array<S1> {

    fn dot(&self, other: Array<S2>) -> Self {
        if self.len() == 1 || other.len() == 1 { self.clone() * other }
        else if self.ndim() == 1 && other.ndim() == 1 {
            assert_eq!(self.len(), other.len(), "Lenghts should match");
            Array::new_elems_flat(vec![sum_product(self.clone(), other)])
        } else if self.ndim() == 2 && other.ndim() == 2 {
            self.matmul(other)
        } else { panic!("Dot product is not defined for given ndims: {} {}", self.ndim(), other.ndim()) }
    }
}
