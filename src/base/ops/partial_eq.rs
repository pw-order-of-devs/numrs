use std::cmp::PartialEq;

use crate::prelude::*;

pub(crate) fn partial_eq<S1: ArrayType, S2: ArrayType>(s1: S1, s2: S2) -> bool {
    s1.to_float() == s2.to_float()
}

impl <S1: ArrayType, S2: ArrayType>PartialEq<ArrayElem<S2>> for ArrayElem<S1> {
    fn eq(&self, other: &ArrayElem<S2>) -> bool {
        self.value.to_string() == other.value.to_string()
    }
}

impl <S: ArrayType>PartialEq<f64> for ArrayElem<S> {
    fn eq(&self, other: &f64) -> bool {
        self.value.to_string() == other.to_string()
    }
}

// PartialEq - scalar

impl <S1: ArrayType, S2: ArrayType>PartialEq<S2> for Array<S1> {
    fn eq(&self, other: &S2) -> bool {
        self.all(|i| partial_eq(i.value, *other), None, None).single().to_bool()
    }
}

impl <S: ArrayType>PartialEq<Array<S>> for f64 {
    fn eq(&self, other: &Array<S>) -> bool {
        other.all(|i| partial_eq(i.value, S::from_float(*self)), None, None).single().to_bool()
    }
}

impl <S: ArrayType>PartialEq<Array<S>> for i32 {
    fn eq(&self, other: &Array<S>) -> bool {
        other.all(|i| partial_eq(i.value, S::from_i32(*self)), None, None).single().to_bool()
    }
}

impl <S: ArrayType>PartialEq<Array<S>> for usize {
    fn eq(&self, other: &Array<S>) -> bool {
        other.all(|i| partial_eq(i.value, S::from_usize(*self)), None, None).single().to_bool()
    }
}

// PartialEq

impl <S1: ArrayType, S2: ArrayType>PartialEq<Array<S2>> for Array<S1> {
    fn eq(&self, other: &Array<S2>) -> bool {
        self.equals(other)
    }
}
