use std::ops::Not;

use crate::prelude::*;

impl Not for ArrayElem<bool> {
    type Output = ArrayElem<bool>;
    fn not(self) -> Self::Output {
        (!self.value).to_elem()
    }
}
