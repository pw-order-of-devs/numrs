use crate::base::ops::sum_product;
use crate::prelude::*;

pub trait Matmul<S1: ArrayType, S2: ArrayType> {
    fn matmul(&self, other: Array<S2>) -> Self;
}

impl <S1: ArrayType, S2: ArrayType>Matmul<S1, S2> for Array<S1> {

    fn matmul(&self, other: Array<S2>) -> Self {
        if self.len() == 1 || other.len() == 1 { self.clone() * other }
        else if self.ndim() == 1 && other.ndim() == 1 {
            assert_eq!(self.len(), other.len(), "Lenghts should match");
            Array::new_elems_flat(vec![sum_product(self.clone(), other)])
        } else if self.ndim() == 2 && other.ndim() == 2 {
            let res_shape = vec![self.get_shape()[0], other.get_shape()[1]];
            if res_shape == vec![1, 1] { return self.ravel().matmul(other.ravel()) };
            let (arrs_1, arrs_2) = (_get_subarrs(self, 1), _get_subarrs(&other, 0));
            let mut res = Array::new(vec![S1::zero(); res_shape.mult()], res_shape.clone());
            for (r, _r) in arrs_1.iter().enumerate().take(res_shape[0]) { for (c, _c) in arrs_2.iter().enumerate().take(res_shape[1]) {
                res.set_element_at(vec![r, c], sum_product(Array::new_flat(_r.clone()), Array::new_flat(_c.clone())));
            } }
            res
        } else { panic!("Matrix multiplication is not defined for given ndims: {} {}", self.ndim(), other.ndim()) }
    }
}

fn _get_subarrs<S: ArrayType>(arr: &Array<S>, axis: usize) -> Vec<Vec<S>> {
    let (arr, arr_len, arrs_count)  = arr.by_axis_vars(axis);
    (0..arrs_count).map(|i| arr.subarray(i * arr_len, (i + 1) * arr_len).get_elements_vec()).collect::<Vec<Vec<S>>>()
}
