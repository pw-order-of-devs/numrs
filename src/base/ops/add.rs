use std::ops::Add;

use crate::prelude::*;

pub(crate) fn add<S1: ArrayType, S2: ArrayType>(s1: S1, s2: S2) -> S1 {
    S1::from_float(s1.to_float() + s2.to_float())
}

impl <S1: ArrayType, S2: ArrayType>Add<ArrayElem<S2>> for ArrayElem<S1> {
    type Output = ArrayElem<S1>;
    fn add(self, other: ArrayElem<S2>) -> Self::Output {
        ArrayElem::from_float(self.value.to_float() + other.value.to_float())
    }
}

// Add - scalar

impl <S1: ArrayType, S2: ArrayType>Add<S2> for Array<S1> {
    type Output = Array<S1>;
    fn add(self, other: S2) -> Self::Output {
        self.map(|s| add(s.value, other).to_elem())
    }
}

impl <S1: ArrayType, S2: ArrayType>Add<ArrayElem<S2>> for Array<S1> {
    type Output = Array<S1>;
    fn add(self, other: ArrayElem<S2>) -> Self::Output {
        self.map(|s| add(s.value, other.value).to_elem())
    }
}

impl <S: ArrayType>Add<Array<S>> for f64 {
    type Output = Array<S>;
    fn add(self, other: Array<S>) -> Self::Output {
        other.map(|s| add(S::from_float(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Add<Array<S>> for i32 {
    type Output = Array<S>;
    fn add(self, other: Array<S>) -> Self::Output {
        other.map(|s| add(S::from_i32(self), s.value).to_elem())
    }
}

impl <S: ArrayType>Add<Array<S>> for usize {
    type Output = Array<S>;
    fn add(self, other: Array<S>) -> Self::Output {
        other.map(|s| add(S::from_usize(self), s.value).to_elem())
    }
}

// Add

impl <S1: ArrayType, S2: ArrayType>Add<Array<S2>> for Array<S1> {
    type Output = Array<S1>;

    fn add(self, other: Array<S2>) -> Self::Output {
        if other.len() == 1 { return self + other.single(); }
        let (arr1, arr2) = Self::broadcast(&self, &other);
        arr1.map_enumerate(|i, s|
            *s + arr2.get_elements()[i]
        )
    }
}
