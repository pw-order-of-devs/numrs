use std::cmp::{Ord, Ordering};

use crate::prelude::*;

pub trait ArrayBroadcast<S: ArrayType> {
    fn broadcast_to(&self, shape: Vec<usize>) -> Self;
    fn broadcast<S1: ArrayType, S2: ArrayType, A1: ArrayBase<S1>, A2: ArrayBase<S2>>(arr1: &A1, arr2: &A2, ) -> (A1, A2);
}

impl <S: ArrayType>ArrayBroadcast<S> for Array<S> {

    fn broadcast_to(&self, shape: Vec<usize>) -> Self {
        assert!(shape_matches(shape.clone(), self.get_shape()), "Shape broadcast must match for this operation");
        let (_, new_shape) = _reshape(self.get_shape(), shape);
        broadcast(self, new_shape)
    }

    fn broadcast<S1: ArrayType, S2: ArrayType, A1: ArrayBase<S1>, A2: ArrayBase<S2>>(arr1: &A1, arr2: &A2, ) -> (A1, A2) {
        assert!(shape_matches(arr1.get_shape(), arr2.get_shape()), "Shape broadcast must match for this operation");
        let (ns1, ns2) = _reshape(arr1.get_shape(), arr2.get_shape());
        (broadcast(arr1, ns2), broadcast(arr2, ns1))
    }
}

fn broadcast<S: ArrayType, A: ArrayBase<S>>(
    arr: &A,
    new_shape: Vec<usize>,
) -> A {
    let (mut arr_shape_rev, mut new_shape_rev) = _reshape(arr.get_shape(), new_shape);
    if arr_shape_rev == new_shape_rev { return arr.update_shape(arr_shape_rev) }
    arr_shape_rev.reverse(); new_shape_rev.reverse();
    let mut new_arr = arr.clone();
    for i in 0 .. new_shape_rev.len() {
        if arr_shape_rev[i] == 1 {
            let mult = new_shape_rev[i] / arr_shape_rev[i];
            let arr_len = Array::new_flat(arr_shape_rev.clone()).subarray(0, i).get_elements_vec().iter().product::<usize>();
            let arrs = (0 .. new_arr.len() / arr_len).map(|i| new_arr.subarray(i * arr_len, (i + 1) * arr_len).get_elements_vec()).collect::<Vec<Vec<S>>>();
            new_arr = ArrayBase::<S>::new_flat(arrs.iter().flat_map(|a| vec![a; mult].into_iter().flatten().cloned().collect::<Vec<S>>()).collect());
            arr_shape_rev[i] = new_shape_rev[i];
        }
    }
    arr_shape_rev.reverse();
    new_arr.update_shape(arr_shape_rev)
}

fn shape_matches(v1: Vec<usize>, v2: Vec<usize>) -> bool {
    if v2.is_empty() || v2 == vec![1] { return true }
    let (v1, v2) = _reshape(v1, v2);
    for i in 0 .. v1.len() { if v1[i] != 1 && v2[i] != 1 && v1[i] != v2[i] { return false } }
    true
}

fn _reshape(v1: Vec<usize>, v2: Vec<usize>) -> (Vec<usize>, Vec<usize>) {
    match v1.len().cmp(&v2.len()) {
        Ordering::Equal => { (v1, v2) },
        Ordering::Greater => { (v1.clone(), _reshape_i(&v2, v1.len(), v2.len())) },
        Ordering::Less => { (_reshape_i(&v1, v2.len(), v1.len()), v2) }
    }
}

fn _reshape_i(v: &[usize], len1: usize, len2: usize) -> Vec<usize> {
    let mut _v = vec![1; len1 - len2];
    _v.extend_from_slice(v);
    _v
}
