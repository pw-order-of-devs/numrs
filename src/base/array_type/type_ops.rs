pub trait ArrayTypeOps {
    fn add(&self, other: Self) -> Self;
    fn sub(&self, other: Self) -> Self;
    fn mul(&self, other: Self) -> Self;
    fn div(&self, other: Self) -> Self;
    fn max(&self, other: Self) -> Self;
    fn min(&self, other: Self) -> Self;
}

impl ArrayTypeOps for usize {
    fn add(&self, other: usize) -> usize { self + other }
    fn sub(&self, other: usize) -> usize { self - other }
    fn mul(&self, other: usize) -> usize { self * other }
    fn div(&self, other: usize) -> usize { self / other }
    fn max(&self, other: usize) -> usize { if *self > other { *self } else { other } }
    fn min(&self, other: usize) -> usize { if *self > other { other } else { *self } }
}

impl ArrayTypeOps for f64 {
    fn add(&self, other: f64) -> f64 { self + other }
    fn sub(&self, other: f64) -> f64 { self - other }
    fn mul(&self, other: f64) -> f64 { self * other }
    fn div(&self, other: f64) -> f64 { self / other }
    fn max(&self, other: f64) -> f64 { if *self > other { *self } else { other } }
    fn min(&self, other: f64) -> f64 { if *self > other { other } else { *self } }
}

impl ArrayTypeOps for i32 {
    fn add(&self, other: i32) -> i32 { self + other }
    fn sub(&self, other: i32) -> i32 { self - other }
    fn mul(&self, other: i32) -> i32 { self * other }
    fn div(&self, other: i32) -> i32 { self / other }
    fn max(&self, other: i32) -> i32 { if *self > other { *self } else { other } }
    fn min(&self, other: i32) -> i32 { if *self > other { other } else { *self } }
}

impl ArrayTypeOps for bool {
    fn add(&self, _: bool) -> bool { panic!("Not implemented!") }
    fn sub(&self, _: bool) -> bool { panic!("Not implemented!") }
    fn mul(&self, _: bool) -> bool { panic!("Not implemented!") }
    fn div(&self, _: bool) -> bool { panic!("Not implemented!") }
    fn max(&self, _: bool) -> bool { panic!("Not implemented!") }
    fn min(&self, _: bool) -> bool { panic!("Not implemented!") }
}
