use std::{
    cmp::{Ordering, PartialEq},
    fmt::{Debug, Display},
    iter::{FromIterator},
};
use crate::base::array_type::type_ops::ArrayTypeOps;

use crate::prelude::*;

#[derive(Clone, Copy, Debug, PartialOrd)]
pub struct ArrayElem<S: ArrayType> {
    pub(crate) value: S,
    pub(crate) mask: usize,
}

impl <S: ArrayType>ArrayElem<S> {
    pub fn value(self) -> S { self.value }
    pub fn mask(self) -> usize { self.mask }
    pub fn get(self) -> Option<S> { if self.mask == 1 { None } else { Some(self.value) } }

    pub fn new(value: S) -> Self { ArrayElem { value, mask: 0 } }
    pub fn new_masked(value: S, mask: usize) -> Self { ArrayElem { value, mask } }

    pub fn zero() -> Self { ArrayElem::new(S::zero()) }
    pub fn one() -> Self { ArrayElem::new(S::one()) }
    pub fn minus_one() -> Self { ArrayElem::new(S::minus_one()) }
    pub fn two() -> Self { ArrayElem::new(S::two()) }
    pub fn nan() -> Self { ArrayElem::new(S::nan()) }
    pub fn inf() -> Self { ArrayElem::new(S::inf()) }
    pub fn neg_inf() -> Self { ArrayElem::new(S::neg_inf()) }

    pub fn from_usize(u: usize) -> Self { ArrayElem::new(S::from_usize(u)) }
    pub fn from_i32(u: i32) -> Self { ArrayElem::new(S::from_i32(u)) }
    pub fn from_float(f: f64) -> Self { ArrayElem::new(S::from_float(f)) }
    pub fn from_bool(b: bool) -> Self { ArrayElem::new(S::from_bool(b)) }
    pub fn to_float(self) -> f64 { self.value.to_float() }
    pub fn to_i32(self) -> i32 { self.value.to_i32() }
    pub  fn to_usize(self) -> usize { self.value.to_usize() }
    pub fn to_bool(self) -> bool { self.value.to_bool() }

    pub fn is_nan(&self) -> bool { self.value.is_nan() }
    pub fn powf(self, exp: S) -> Self { ArrayElem::new_masked(self.value.powf(exp), self.mask) }
    pub fn sqrt(self) -> Self { ArrayElem::new_masked(self.value.sqrt(), self.mask) }
    pub fn abs(self) -> Self { ArrayElem::new_masked(self.value.abs(), self.mask) }
    pub fn exp(self) -> Self { ArrayElem::new_masked(self.value.exp(), self.mask) }
    pub fn ln(self) -> Self { ArrayElem::new_masked(self.value.ln(), self.mask) }
    pub fn equals(&self, other: &ArrayElem<S>) -> bool { self.value == other.value }
    pub fn lt(&self, other: &ArrayElem<S>) -> bool { self.value < other.value }
    pub fn lte(&self, other: &ArrayElem<S>) -> bool { self.value <= other.value }
    pub fn gt(&self, other: &ArrayElem<S>) -> bool { self.value > other.value }
    pub fn gte(&self, other: &ArrayElem<S>) -> bool { self.value >= other.value }
    pub fn ord(&self, other: &ArrayElem<S>) -> Ordering {
        if self.gt(other) { Ordering::Greater }
        else if self.lt(other) { Ordering::Less }
        else { Ordering::Equal }
    }
}

impl <S: ArrayType>FromIterator<ArrayElem<S>> for Vec<S> {
    fn from_iter<T: IntoIterator<Item=ArrayElem<S>>>(iter: T) -> Self {
        let mut vec = vec![];
        for i in iter { vec.push(i.value) }
        vec
    }
}

pub trait ArrayType:
    Copy + Sized + Debug + Display
    + PartialEq + PartialOrd
    + ArrayTypeConv + ArrayTypeOps {
    fn powf(self, exp: Self) -> Self;
    fn sqrt(self) -> Self;
    fn abs(self) -> Self;
    fn exp(self) -> Self;
    fn ln(self) -> Self;
    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn tan(self) -> Self;
    fn is_nan(self) -> bool;
    fn zero() -> Self;
    fn one() -> Self;
    fn minus_one() -> Self;
    fn two() -> Self;
    fn nan() -> Self;
    fn inf() -> Self;
    fn neg_inf() -> Self;
}

impl ArrayType for f64 {
    fn powf(self, exp: f64) -> f64 { f64::powf(self, exp) }
    fn sqrt(self) -> f64 { f64::sqrt(self) }
    fn abs(self) -> f64 { f64::abs(self) }
    fn exp(self) -> f64 { f64::exp(self) }
    fn ln(self) -> f64 { f64::ln(self) }
    fn sin(self) -> f64 { f64::sin(self) }
    fn cos(self) -> f64 { f64::cos(self) }
    fn tan(self) -> f64 { f64::tan(self) }
    fn is_nan(self) -> bool { f64::is_nan(self) }
    fn zero() -> f64 { 0. }
    fn one() -> f64 { 1. }
    fn minus_one() -> f64 { -1. }
    fn two() -> f64 { 2. }
    fn nan() -> f64 { f64::NAN }
    fn inf() -> f64 { f64::INFINITY }
    fn neg_inf() -> f64 { f64::NEG_INFINITY }
}

impl ArrayType for i32 {
    fn powf(self, _: i32) -> i32 { panic!("Not implemented!") }
    fn sqrt(self) -> i32 { panic!("Not implemented!") }
    fn abs(self) -> i32 { self }
    fn exp(self) -> i32 { panic!("Not implemented!") }
    fn ln(self) -> i32 { panic!("Not implemented!") }
    fn sin(self) -> i32 { panic!("Not implemented!") }
    fn cos(self) -> i32 { panic!("Not implemented!") }
    fn tan(self) -> i32 { panic!("Not implemented!") }
    fn is_nan(self) -> bool { false }
    fn zero() -> i32 { 0 }
    fn one() -> i32 { 1 }
    fn minus_one() -> i32 { panic!("Not implemented!") }
    fn two() -> i32 { 2 }
    fn nan() -> i32 { panic!("Not implemented!") }
    fn inf() -> i32 { panic!("Not implemented!") }
    fn neg_inf() -> i32 { panic!("Not implemented!") }
}

impl ArrayType for usize {
    fn powf(self, _: usize) -> usize { panic!("Not implemented!") }
    fn sqrt(self) -> usize { panic!("Not implemented!") }
    fn abs(self) -> usize { self }
    fn exp(self) -> usize { panic!("Not implemented!") }
    fn ln(self) -> usize { panic!("Not implemented!") }
    fn sin(self) -> usize { panic!("Not implemented!") }
    fn cos(self) -> usize { panic!("Not implemented!") }
    fn tan(self) -> usize { panic!("Not implemented!") }
    fn is_nan(self) -> bool { false }
    fn zero() -> usize { 0 }
    fn one() -> usize { 1 }
    fn minus_one() -> usize { panic!("Not implemented!") }
    fn two() -> usize { 2 }
    fn nan() -> usize { panic!("Not implemented!") }
    fn inf() -> usize { panic!("Not implemented!") }
    fn neg_inf() -> usize { panic!("Not implemented!") }
}

impl ArrayType for bool {
    fn powf(self, _: bool) -> bool { panic!("Not implemented!") }
    fn sqrt(self) -> bool { panic!("Not implemented!") }
    fn abs(self) -> bool { panic!("Not implemented!") }
    fn exp(self) -> bool { panic!("Not implemented!") }
    fn ln(self) -> bool { panic!("Not implemented!") }
    fn sin(self) -> bool { panic!("Not implemented!") }
    fn cos(self) -> bool { panic!("Not implemented!") }
    fn tan(self) -> bool { panic!("Not implemented!") }
    fn is_nan(self) -> bool { false }
    fn zero() -> bool { false }
    fn one() -> bool { true }
    fn minus_one() -> bool { panic!("Not implemented!") }
    fn two() -> bool { panic!("Not implemented!") }
    fn nan() -> bool { panic!("Not implemented!") }
    fn inf() -> bool { panic!("Not implemented!") }
    fn neg_inf() -> bool { panic!("Not implemented!") }
}
