use crate::prelude::*;

pub(crate) trait ArrayElemsConv<S: ArrayType> {
    fn parse(&self, mask: Option<Vec<usize>>) -> Vec<ArrayElem<S>>;
}

impl <S: ArrayType>ArrayElemsConv<S> for Vec<S> {
    fn parse(&self, mask: Option<Vec<usize>>) -> Vec<ArrayElem<S>> {
        let mask = mask.unwrap_or_else(|| vec![0; self.len()]);
        assert_eq!(self.len(), mask.len(), "Elements count must match mask size");
        (0..self.len()).map(|i| ArrayElem { value: self[i], mask: mask[i] }).collect()
    }
}

pub trait ArrayTypeConv {
    fn to_elem(&self) -> ArrayElem<Self> where Self: ArrayType { ArrayElem::new(*self) }
    fn from_usize(u: usize) -> Self;
    fn from_i32(u: i32) -> Self;
    fn from_float(f: f64) -> Self;
    fn from_bool(b: bool) -> Self;
    fn to_float(self) -> f64;
    fn to_i32(self) -> i32;
    fn to_usize(self) -> usize;
    fn to_bool(self) -> bool;
}

impl ArrayTypeConv for f64 {
    fn from_usize(u: usize) -> f64 { u as f64 }
    fn from_i32(u: i32) -> f64 { u as f64 }
    fn from_float(f: f64) -> f64 { f }
    fn from_bool(b: bool) -> f64 { if b { 1. } else { 0. } }
    fn to_float(self) -> f64 { self as f64 }
    fn to_i32(self) -> i32 { self as i32 }
    fn to_usize(self) -> usize { self as usize }
    fn to_bool(self) -> bool {
        if self.approx_eq(0.) { false }
        else if self.approx_eq(1.) { true }
        else { panic!("Not a bool value") }
    }
}

impl ArrayTypeConv for i32 {
    fn from_usize(u: usize) -> i32 { u as i32 }
    fn from_i32(u: i32) -> i32 { u }
    fn from_float(f: f64) -> i32 { f as i32 }
    fn from_bool(b: bool) -> i32 { i32::from(b) }
    fn to_float(self) -> f64 { self as f64 }
    fn to_i32(self) -> i32 { self as i32 }
    fn to_usize(self) -> usize { self as usize }
    fn to_bool(self) -> bool { match self { 0 => false, 1 => true, _ => panic!("Not a bool value"), } }
}

impl ArrayTypeConv for usize {
    fn from_usize(u: usize) -> usize { u }
    fn from_i32(u: i32) -> usize { u as usize }
    fn from_float(f: f64) -> usize { f as usize }
    fn from_bool(b: bool) -> usize { usize::from(b) }
    fn to_float(self) -> f64 { self as f64 }
    fn to_i32(self) -> i32 { self as i32 }
    fn to_usize(self) -> usize { self as usize }
    fn to_bool(self) -> bool { match self { 0 => false, 1 => true, _ => panic!("Not a bool value"), } }
}

impl ArrayTypeConv for bool {
    fn from_usize(u: usize) -> bool { match u { 0 => false, 1 => true, _ => panic!("Not a bool value"), } }
    fn from_i32(u: i32) -> bool { match u { 0 => false, 1 => true, _ => panic!("Not a bool value"), } }
    fn from_float(f: f64) -> bool { match f.to_usize() { 0 => false, 1 => true, _ => panic!("Not a bool value"), } }
    fn from_bool(b: bool) -> bool { b }
    fn to_float(self) -> f64 { match self { false => 0., true => 1., } }
    fn to_i32(self) -> i32 { self as i32 }
    fn to_usize(self) -> usize { self as usize }
    fn to_bool(self) -> bool { self }
}
