use std::iter::FromIterator;
use std::vec::IntoIter;

use crate::prelude::*;

impl <S: ArrayType>IntoIterator for Array<S> {
    type Item = ArrayElem<S>;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.get_elements().into_iter()
    }
}

impl <S: ArrayType>FromIterator<ArrayElem<S>> for Array<S> {
    fn from_iter<T: IntoIterator<Item=ArrayElem<S>>>(iter: T) -> Self {
        Array::new_elems_flat(iter.into_iter().collect())
    }
}
