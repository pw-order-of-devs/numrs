use crate::prelude::*;

const BLOCK: usize = 65546;

enum SearchSorted { Left, Right, }

pub struct HistBins {
    pub bin: Option<usize>,
    pub bins: Option<Vec<usize>>,
}

impl HistBins {
    pub fn single(bin: usize) -> Self { HistBins { bin: Some(bin), bins: None } }
    pub fn multi(bins: Vec<usize>) -> Self { HistBins { bin: None, bins: Some(bins) } }
}

pub trait ArrayHist<S: ArrayType>: Sized {
    fn histogram(
        &self,
        bins: Option<HistBins>,
        range: Option<(S, S)>,
        weights: Option<Self>,
        density: Option<bool>
    ) -> (Self, Self);

    fn bincount(
        &self,
        weights: Option<Array<S>>,
        min_lenght: usize,
    ) -> Array<S>;
}

impl <S: ArrayType>ArrayHist<S> for Array<S> {

    fn histogram(
        &self,
        bins: Option<HistBins>,
        range: Option<(S, S)>,
        weights: Option<Array<S>>,
        density: Option<bool>
    ) -> (Array<S>, Array<S>) {
        let (arr, _) = (self.ravel(), if let Some(w) = weights.clone() {
            assert_eq!(self.get_shape(), w.get_shape(), "Weights should have the same shape as array.");
            Some(w.ravel())
        } else { None });
        let (bin_edges, uniform_bins): (Array<S>, Option<(S, S, usize)>) = find_bin_edges(self, bins, range);
        let n: Array<S> =
            if let Some(ub) = uniform_bins { bincount_method(&arr, ub, weights, &bin_edges) }
            else { cumulative_method(&arr, weights, bin_edges.clone()) };
        if density.unwrap_or(false) { (n.clone() / bin_edges.diff(None) / n.sum(None, None).single(), bin_edges) }
        else { (n, bin_edges) }
    }

    fn bincount(
        &self,
        weights: Option<Array<S>>,
        min_lenght: usize,
    ) -> Array<S> {
        let indices = self.get_elements_vec().into_iter().map(|i| i.to_i32()).collect::<Vec<i32>>();
        let weights = if let Some(w) = weights { w } else { Array::ones(vec![indices.len()]) };
        let mut unique = indices.clone();
        unique.sort_unstable();
        unique.dedup();
        let mut res = unique.into_iter()
            .map(|v| weights.clone().filter_enumerate(|j, _| indices[j] == v).sum(None, None).single().value)
            .collect::<Vec<S>>();
        if res.len() < min_lenght { (0..min_lenght - res.len()).for_each(|_| res.push(S::zero())); }
        Array::new_flat(res)
    }
}

fn filter_by_keep<S: ArrayType>(vec: Array<S>, keep: Array<bool>) -> Array<S> {
    vec.filter_enumerate(|i, _| keep.at_v(vec![i]).value.to_bool())
}

fn bincount_method<S: ArrayType>(
    arr: &Array<S>,
    ub: (S, S, usize),
    weights: Option<Array<S>>,
    bin_edges: &Array<S>,
) -> Array<S> {
    let (first_edge, last_edge, n_equal_bins) = ub;
    let mut n = Array::zeros(vec![n_equal_bins]);
    let norm = S::from_usize(n_equal_bins).div(last_edge.sub(first_edge));

    (0..arr.len()).step_by(BLOCK).for_each(|i| {
        let mut tmp_a = arr.subarray(i, std::cmp::min(arr.len(), i + BLOCK));
        let mut tmp_w = weights.clone().map(|w| w.subarray(i, std::cmp::min(arr.len(), i + BLOCK)));

        let tmp_a_mapped = tmp_a
            .get_elements().into_iter()
            .map(|i| (i.value.ge(&first_edge)) & (i.value.le(&last_edge))).collect::<Vec<bool>>();
        let keep: Array<bool> = Array::new(tmp_a_mapped, tmp_a.get_shape());
        if !keep.fold(true.to_elem(), |a, b| *a & *b).value {
            tmp_a = filter_by_keep(tmp_a, keep.clone());
            if let Some(tw) = tmp_w { tmp_w = Some(filter_by_keep(tw, keep)); }
        }
        let indices = tmp_a.clone().into_iter()
            .map(|i| ((i - first_edge.to_elem()) * norm.to_elem()).value.to_i32())
            .map(|i| if i == n_equal_bins as i32 { i - 1 } else { i })
            .collect::<Vec<i32>>();
        let bin_edges_mapped_0 = indices.clone().into_iter()
            .map(|i| bin_edges.get_elements()[if i < 0 { bin_edges.len() - (i.unsigned_abs() as usize) } else { i as usize }])
            .collect::<Vec<S>>();
        let decrement = (0..tmp_a.len()).map(|i| tmp_a.at_v(vec![i]) < bin_edges_mapped_0[i].to_elem()).collect::<Vec<bool>>();
        let indices = (0..indices.len()).map(|i| indices[i] - i32::from(decrement[i])).collect::<Vec<i32>>();
        let bin_edges_mapped_1 = indices.clone().into_iter()
            .map(|i| bin_edges.get_elements()[if i < 0 { bin_edges.len() - (i.unsigned_abs() as usize) } else { i as usize } + 1])
            .collect::<Vec<S>>();
        let increment = (0..tmp_a.len())
            .map(|i| (tmp_a.at_v(vec![i]) >= bin_edges_mapped_1[i].to_elem()) & (indices[i] != (n_equal_bins - 1) as i32))
            .collect::<Vec<bool>>();
        let indices = (0..indices.len()).map(|i| indices[i] + i32::from(increment[i])).map(S::from_i32).collect::<Vec<S>>();
        n = n.clone() + Array::new_flat(indices).bincount(tmp_w, n_equal_bins);
    });
    n
}

fn cumulative_method<S: ArrayType>(arr: &Array<S>, weights: Option<Array<S>>, bin_edges: Array<S>) -> Array<S> {
    let mut cum_n = Array::zeros(bin_edges.get_shape());
    match weights {
        None => (0..arr.len()).step_by(BLOCK).for_each(|i| {
            let sa = arr.subarray(i, std::cmp::min(arr.len(), i + BLOCK)).sort(None);
            let found: Array<S> = search_sorted_inclusive(&sa, bin_edges.clone());
            cum_n = cum_n.clone() + found
        }),
        Some(w) => {
            let zero = Array::zeros(vec![1]);
            (0..arr.len()).step_by(BLOCK).for_each(|i| {
                let tmp_a = arr.subarray(i, std::cmp::min(arr.len(), i + BLOCK));
                let tmp_w = w.subarray(i, std::cmp::min(w.len(), i + BLOCK));
                let sorting_index = tmp_a.argsort(None);
                let sa = Array::new_flat(sorting_index.clone().into_iter()
                    .map(|i| cumulative_method_map_fn(&tmp_a, i))
                    .collect::<Vec<S>>());
                let sw = Array::new_flat(sorting_index.into_iter()
                    .map(|i| cumulative_method_map_fn(&tmp_w, i))
                    .collect::<Vec<S>>());
                let mut cw = zero.get_elements();
                cw.extend(sw.cumsum(None));
                let cw = Array::new_elems_flat(cw);
                let bin_index: Array<S> = search_sorted_inclusive(&sa, bin_edges.clone());
                cum_n = cum_n.clone() + Array::new_flat(bin_index.into_iter()
                    .map(|i| cumulative_method_map_fn(&cw, i))
                    .collect::<Vec<S>>()
                );
            });
        },
    };
    cum_n.diff(None)
}

fn cumulative_method_map_fn<S: ArrayType>(arr: &Array<S>, i: ArrayElem<S>) -> ArrayElem<S> {
    arr.get_elements()[(if i.value < S::zero() { S::from_usize(arr.len()).sub(i.value.abs()) } else { i.value }).to_usize()]
}

fn find_bin_edges<S: ArrayType>(arr: &Array<S>, bins: Option<HistBins>, range: Option<(S, S)>) -> (Array<S>, Option<(S, S, usize)>) {
    let bins = bins.unwrap_or_else(|| HistBins::single(10));
    let (n_equal_bins, mut edges) = if let Some(b) = bins.bin {
        assert!(b > 0, "Bin value must be positive if an integer.");
        let bin_edges = find_outer_edges(arr, range);
        (Some(b), Array::new_flat(vec![bin_edges.0, bin_edges.1]))
    } else if let Some(b) = bins.bins {
        let bin_edges = b.into_iter().map(S::from_usize).collect::<Vec<S>>();
        (0..bin_edges.len() - 2).for_each(|i| assert!(bin_edges[i] < bin_edges[i + 1], "Bins value must increase monotonically if an array"));
        (None, Array::new_flat(bin_edges))
    } else { panic!("Bin value must be one of integer or array.") };
    if let Some(neb) = n_equal_bins {
        let (f, l) = (edges.get_elements()[0].value, edges.get_elements()[1].value);
        edges = Array::new_flat((0..neb + 1).map(|i| f.add(l.sub(f).mul(S::from_usize(i)).div(S::from_usize(neb)))).collect::<Vec<S>>());
        (edges, Some((f, l, neb)))
    } else {
        (edges, None)
    }
}

fn find_outer_edges<S: ArrayType>(arr: &Array<S>, range: Option<(S, S)>) -> (S, S) {
    let (mut first_edge, mut last_edge) =
        if let Some(r) = range { r } else if arr.is_empty() { (S::zero(), S::one()) } else {
            let mut elems = arr.get_elements();
            elems.sort_by(|a, b| a.ord(b));
            (elems[0].value, elems[elems.len() - 1].value)
        };
    if first_edge > last_edge { panic!("max must be larger than min in range parameter.") }
    if first_edge.to_float().is_infinite() || last_edge.to_float().is_infinite() { panic!("supplied range is not finite.") }
    if first_edge == last_edge {
        first_edge = first_edge.sub(S::from_float(0.5));
        last_edge = last_edge.add(S::from_float(0.5));
    }
    (first_edge, last_edge)
}

fn search_sorted_inclusive<S: ArrayType>(arr: &Array<S>, bin_edges: Array<S>) -> Array<S> {
    let mut res = search_sorted(arr, bin_edges.subarray(0, bin_edges.len() - 1), Some(SearchSorted::Left)).get_elements();
    res.extend(search_sorted(arr, bin_edges.subarray(bin_edges.len() - 1, bin_edges.len()), Some(SearchSorted::Right)));
    Array::new_elems_flat(res)
}

fn search_sorted<S: ArrayType>(arr: &Array<S>, bin_edges: Array<S>, side: Option<SearchSorted>) -> Array<S> {
    let side = side.unwrap_or(SearchSorted::Left);

    Array::new_flat(
        (0..bin_edges.len()).map(|i| {
            let a = arr.get_elements();
            let i_pre = if i == 0 { 0 } else { i - 1 };
            bin_edges.filter(|be| match side {
                SearchSorted::Left => a[i_pre] < *be && *be <= a[i],
                SearchSorted::Right => a[i_pre] <= *be && *be < a[i],
            })
        }).map(|r| {
            let elems = r.get_elements();
            if r.len() > 0 { elems[0] } else {
                ArrayElem {
                    value: match side {
                        SearchSorted::Left => S::zero(),
                        SearchSorted::Right => S::from_usize(arr.len()),
                    },
                    mask: 0,
                }
            }
        }).collect()
    )
}
