pub(crate) use crate::base::{
    array_type::type_conv::ArrayElemsConv,
};

pub use crate::base::{
    array::ArrayBase,
    array_type::{
        type_def::{ArrayElem, ArrayType},
        type_conv::ArrayTypeConv,
        type_ops::ArrayTypeOps,
    },
    axis::ArrayAxis,
    broadcast::ArrayBroadcast,
    display::ArrayDisplay,
    hist::{ArrayHist, HistBins},
    limits::ArrayLimits,
    ops::{ArrayOps, at::At, dot::Dot, matmul::Matmul},
    transpose::ArrayTranspose,
    types::norm::{Ord, OrdType},
};

use crate::arrays::array::Array;

pub trait ArrayImpl<S>: std::fmt::Display + Clone + IntoIterator<Item=ArrayElem<S>>
    + ArrayBase<S> + ArrayBroadcast<S> + ArrayOps<S> + ArrayAxis<S>
    + ArrayHist<S> + ArrayLimits<S> + ArrayTranspose<S>
    where S: ArrayType {}

impl <S: ArrayType>ArrayImpl<S> for Array<S> {}