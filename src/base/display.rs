use std::fmt::{Display, Formatter, Result};

use crate::prelude::*;

impl <S: ArrayType>Display for ArrayElem<S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        if self.mask == 0 { write!(f, "{:.4}", self.value) }
        else { write!(f, "------") }
    }
}

impl <S: ArrayType>Display for Array<S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        self.fmt_array(f)
    }
}

pub trait ArrayDisplay<S: ArrayType> {
    fn build_string(&self, pretty: Option<bool>) -> String;
    fn get_element_repr_at(&self, idx: usize) -> String;
    fn fmt_array(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result;
}

impl <S: ArrayType>ArrayDisplay<S> for Array<S> {

    fn build_string(&self, pretty: Option<bool>) -> String {
        if self.is_empty() { return "[]".to_string() }
        else if self.len() == 1 { return format!("[{}]", self.get_element_repr_at(0)) }
        let pretty = if let Some(p) = pretty { p } else { false };
        let mut shape = self.get_shape();
        shape.reverse();
        let mut mults = shape.clone().into_iter().enumerate()
            .map(|(idx, _)| shape[0..idx + 1].to_vec().mult())
            .collect::<Vec<usize>>();
        let (mut str, len_first) = (vec![], shape[0]);
        mults.reverse();
        str.push("[".repeat(mults.len()));
        self.get_elements().into_iter().enumerate().for_each(|(idx, _)| {
            if idx == 0 && self.get_shape()[self.ndim() - 1] != 1 { str.push(format!("{}, ", self.get_element_repr_at(idx))) }
            else if idx == 0 { str.push(self.get_element_repr_at(idx)) }
            else {
                let separators = mults.clone().into_iter().map(|s| idx % s == 0).filter(|i| *i).count();
                str.push("]".repeat(separators));
                if separators > 0 {
                    str.push(", ".to_string());
                    if pretty {
                        str.push("\n".to_string());
                        str.push(" ".repeat(self.get_shape().len() - separators));
                    }
                }
                str.push("[".repeat(separators));
                if idx % len_first != len_first - 1 { str.push(format!("{}, ", self.get_element_repr_at(idx))); }
                else { str.push(self.get_element_repr_at(idx)); }
            }
        });
        mults.into_iter().for_each(|_| str.push("]".to_string()));
        str.join("")
    }

    fn get_element_repr_at(&self, idx: usize) -> String {
        format!("{}", self.at(idx))
    }

    fn fmt_array(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if f.alternate() { write!(f, "{}", self.build_string(Some(true))) }
        else { write!(f, "{}", self.build_string(None)) }
    }
}
