use std::ops::Range;
use crate::ext::vec::lexsort;
use crate::prelude::*;

#[derive(Clone, Debug, PartialOrd)]
pub struct Array<S: ArrayType> {
    pub(crate) elements: Vec<ArrayElem<S>>,
    pub(crate) shape: Vec<usize>,
}

impl <S: ArrayType>ArrayBase<S> for Array<S> {

    fn new(values: Vec<S>, shape: Vec<usize>) -> Array<S> {
        assert_eq!(values.len(), shape.mult(), "Shape must match values length");
        Array { elements: values.parse(None), shape, }
    }
    fn new_flat(values: Vec<S>) -> Array<S> {
        Array { elements: values.parse(None), shape: vec![values.len()], }
    }

    fn new_masked(values: Vec<S>, shape: Vec<usize>, mask: Vec<usize>) -> Array<S> {
        assert_eq!(values.len(), shape.mult(), "Shape must match values length");
        assert_eq!(values.len(), mask.len(), "Mask must match values length");
        assert!(validate_mask(&mask), "Mask values must be ones of [0, 1]");
        Array { elements: values.parse(Some(mask)), shape }
    }

    fn new_masked_flat(values: Vec<S>, mask: Vec<usize>) -> Array<S> {
        assert_eq!(values.len(), mask.len(), "Mask must match values length");
        assert!(validate_mask(&mask), "Mask values must be ones of [0, 1]");
        Array { elements: values.parse(Some(mask)), shape: vec![values.len()], }
    }

    fn new_elems(elements: Vec<ArrayElem<S>>, shape: Vec<usize>) -> Array<S> {
        assert_eq!(elements.len(), shape.mult(), "Shape must match values length");
        assert!(validate_mask(&elements.iter().map(|i| i.mask()).collect::<Vec<usize>>()), "Mask values must be ones of [0, 1]");
        Array { elements, shape }
    }

    fn new_elems_flat(elements: Vec<ArrayElem<S>>) -> Array<S> {
        assert!(validate_mask(&elements.iter().map(|i| i.mask()).collect::<Vec<usize>>()), "Mask values must be ones of [0, 1]");
        Array { elements: elements.clone(), shape: vec![elements.len()] }
    }

    fn arange(range: Range<usize>, step: Option<usize>) -> Self {
        Array::new_flat(range.step_by(step.unwrap_or(1)).map(S::from_usize).collect())
    }

    fn clone_arr(arr: &impl ArrayBase<S>) -> Array<S> {
        Array::new_elems(arr.get_elements(), arr.get_shape())
    }

    fn empty() -> Array<S> {
        Array::new_flat(vec![])
    }

    fn to_f64_arr(&self) -> Array<f64> {
        let elems = self.get_elements_vec().iter().map(|i| i.to_float()).collect();
        let shape = self.get_shape();
        Array::new(elems, shape)
    }

    fn to_usize_arr(&self) -> Array<usize> {
        let elems = self.get_elements_vec().iter().map(|i| i.to_usize()).collect();
        let shape = self.get_shape();
        Array::new(elems, shape)
    }

    fn to_i32_arr(&self) -> Array<i32> {
        let elems = self.get_elements_vec().iter().map(|i| i.to_i32()).collect();
        let shape = self.get_shape();
        Array::new(elems, shape)
    }

    fn to_bool_arr(&self) -> Array<bool> {
        let elems = self.get_elements_vec().iter().map(|i| i.to_bool()).collect();
        let shape = self.get_shape();
        Array::new(elems, shape)
    }

    fn zeros(shape: Vec<usize>) -> Array<S> {
        Array::new_masked(vec![S::zero(); shape.mult()], shape.clone(), vec![0; shape.mult()])
    }

    fn ones(shape: Vec<usize>) -> Array<S> {
        Array::new_masked(vec![S::one(); shape.mult()], shape.clone(), vec![0; shape.mult()])
    }

    fn ravel(&self) -> Array<S> {
        self.update_shape(vec![self.len()])
    }

    fn subarray(&self, from: usize, to: usize) -> Array<S> {
        Array::new_elems_flat(self.get_elements()[from..to].to_vec())
    }

    fn get_elements(&self) -> Vec<ArrayElem<S>> {
        self.elements.clone()
    }

    fn get_elements_vec(&self) -> Vec<S> {
        self.get_elements().into_iter().map(|i| i.value).collect()
    }

    fn get_shape(&self) -> Vec<usize> {
        self.shape.clone()
    }

    fn get_mask(&self) -> Vec<usize> {
        self.get_elements().into_iter().map(|i| i.mask).collect()
    }

    fn update_elements(&self, elements: Vec<ArrayElem<S>>) -> Array<S> {
        Array::new_elems(elements, self.shape.to_vec())
    }

    fn update_shape(&self, shape: Vec<usize>) -> Array<S> {
        Array::new(self.elements.iter().map(|m| m.value).collect(), shape)
    }

    fn set_elements(&mut self, elements: Vec<ArrayElem<S>>) {
        assert_eq!(elements.len(), self.shape.mult(), "Shape must match values length");
        self.elements = elements
    }

    fn set_shape(&mut self, shape: Vec<usize>) {
        assert_eq!(self.elements.len(), shape.mult(), "Shape must match values length");
        self.shape = shape
    }

    fn ndim(&self) -> usize {
        self.get_shape().len()
    }

    fn len(&self) -> usize {
        self.get_elements().len()
    }

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    fn values_len(&self) -> usize {
        <&Self>::clone(&self).filter_by_mask().len()
    }

    fn contains_nan(&self) -> bool {
        self.get_elements().into_iter().any(|i| i.is_nan())
    }

    fn filter_by_mask(&self) -> Array<S> {
        let arr = Array::new_elems_flat(self.get_elements().into_iter().filter(|i| i.mask == 0).collect());
        if arr.is_empty() { Array::new_elems_flat(vec![ArrayElem::new_masked(S::zero(), 1)]) } else { arr }
    }

    fn filter<F: FnMut(&ArrayElem<S>) -> bool>(&self, mut f: F) -> Array<S> {
        Array::new_elems_flat(
            self.filter_by_mask().get_elements()
            .into_iter()
            .filter(|s| f(s))
            .collect())
    }

    fn filter_enumerate<F: FnMut(usize, &ArrayElem<S>) -> bool>(&self, mut f: F) -> Array<S> {
        Array::new_elems_flat(
            self.filter_by_mask().get_elements()
                .into_iter().enumerate()
                .filter(|(i, s)| f(*i, s))
                .map(|i| i.1)
                .collect()
        )
    }

    fn map<F: FnMut(&ArrayElem<S>) -> ArrayElem<S>>(&self, mut f: F) -> Array<S> {
        self.update_elements(self.get_elements().iter().map(|s| if s.mask() == 0 { f(s) } else { *s }).collect())
    }

    fn map_enumerate<F: FnMut(usize, &ArrayElem<S>) -> ArrayElem<S>>(&self, mut f: F) -> Array<S> {
        self.update_elements(self.get_elements().iter().enumerate().map(|(i, s)| if s.mask() == 0 { f(i, s) } else { *s }).collect())
    }

    fn for_each<F: FnMut(&ArrayElem<S>)>(&self, mut f: F) {
        self.filter_by_mask().get_elements().into_iter().for_each(|s| f(&s))
    }

    fn for_each_enumerate<F: FnMut(usize, &ArrayElem<S>)>(&self, mut f: F) {
        self.filter_by_mask().get_elements().into_iter().enumerate().for_each(|(i, s)| f(i, &s))
    }

    fn fold<F: FnMut(&ArrayElem<S>, &ArrayElem<S>) -> ArrayElem<S>>(&self, init: ArrayElem<S>, mut f: F) -> ArrayElem<S> {
        let filtered = self.filter_by_mask();
        if filtered.all(|i| i.mask() == 1, None, None).single().to_bool() { ArrayElem::new_masked(init.value(), 1) }
        else { filtered.get_elements().into_iter().fold(init, |a, b| f(&a, &b)) }
    }

    fn clip(&self, min: Option<S>, max: Option<S>) -> Array<S> {
        if min.is_none() && max.is_none() { return self.clone() };
        if min.is_some() && max.is_some() { assert!(min.unwrap() <= max.unwrap(), "min value can't be greater than max value") };
        self.map(|i| {
            let (min, max) = (min.unwrap_or_else(|| i.value()), max.unwrap_or_else(|| i.value()));
            if i.value() < min { min.to_elem() } else if i.value() > max { max.to_elem() } else { *i }
        })
    }

    fn diag(&self, offset: Option<usize>) -> Array<S> {
        let offset = offset.unwrap_or(0);
        match self.ndim() {
            1 => {
                let mut diagonal = Array::zeros(vec![self.len() + offset, self.len() + offset]);
                self.for_each_enumerate(|i, v| diagonal.set_element_at(vec![i, i + offset], *v));
                diagonal
            },
            2 => {
                if offset > *self.get_shape().iter().min().unwrap_or(&0) { return Array::empty() };
                let arr = if offset > 0 { make_square(&push_by_offset(self, offset)) } else { self.clone() };
                let mut diagonal = vec![];
                (0 .. arr.get_shape()[0]).for_each(|i| diagonal.push(arr.at_v(vec![i, i])));
                Array::new_elems_flat(diagonal)
            },
            _ => panic!("diag is supported for 1- and 2-d arrays"),
        }
    }

    fn get_element_at(&self, index: Vec<usize>) -> ArrayElem<S> {
        self.get_elements()[Array::<S>::index_of(self.get_shape(), index)]
    }

    fn update_element_at(&self, index: Vec<usize>, element: ArrayElem<S>) -> Array<S> {
        let mut elems = self.get_elements();
        elems[Array::<S>::index_of(self.get_shape(), index)] = element;
        self.update_elements(elems)
    }

    fn set_element_at(&mut self, index: Vec<usize>, element: ArrayElem<S>) {
        let mut elems = self.get_elements();
        elems[Array::<S>::index_of(self.get_shape(), index)] = element;
        self.elements = elems
    }

    fn by_idx_flat(&self, idx: usize) -> ArrayElem<S> {
        self.get_elements()[idx]
    }

    fn index_of(shape: Vec<usize>, index: Vec<usize>) -> usize {
        assert_eq!(shape.len(), index.len(), "Index doesn't match the shape");
        shape.clone().into_iter().enumerate().for_each(|(idx, i)| assert!(i > index[idx], "Index doesn't match the shape"));
        let last = shape.len() - 1;
        if index.len() == 1 { index[0] }
        else { index[last] + shape[last] * Array::<S>::index_of(shape[0..last].to_vec(), index[0..last].to_vec()) }
    }

    fn single(&self) -> ArrayElem<S> {
        assert_eq!(self.len(), 1, "Only for single-element arrays");
        *self.get_elements().first().unwrap()
    }

    fn equals<S1: ArrayType>(&self, other: &impl ArrayBase<S1>) -> bool {
        let (arr1, arr2) = Array::<S>::broadcast(self, other);
        (0..arr1.len()).all(|i|
            arr1.get_elements()[i] == arr2.get_elements()[i]
        )
    }

    fn equals_elems<S1: ArrayType>(&self, other: &impl ArrayBase<S1>) -> Array<S> {
        let (arr1, arr2) = Array::<S>::broadcast(self, other);
        arr1.map_enumerate(|i, v|
            if *v == arr2.get_elements()[i] { ArrayElem::one() } else { ArrayElem::zero() }
        )
    }

    fn lexsort<S1: ArrayType>(&self, other: &impl ArrayBase<S1>) -> Array<usize> {
        lexsort(self, &Array::new_elems_flat(other.get_elements()))
    }
}

fn validate_mask(mask: &[usize]) -> bool {
    !mask.iter().any(|x| vec![0, 1].contains(x).not())
}

fn push_by_offset<S: ArrayType>(arr: &Array<S>, offset: usize) -> Array<S> {
    assert_eq!(arr.ndim(), 2, "defined only for 2d arrays");
    if offset >= arr.get_shape()[0] { return Array::empty() };
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(arr.ndim() - 1);
    let arrs = (0..arrs_count)
        .map(|i| by_axis.subarray(i * arr_len, (i + 1) * arr_len).sort(None))
        .flat_map(|i| i.filter_enumerate(|i, _| i >= offset))
        .collect::<Vec<S>>();
    let mut shape = arr.get_shape();
    shape[arr.ndim() - 1] -= offset;
    Array::new(arrs, shape)
}

fn make_square<S: ArrayType>(arr: &Array<S>) -> Array<S> {
    assert_eq!(arr.ndim(), 2, "defined only for 2d arrays");
    if arr.shape.iter().all(|v| *v == arr.shape[0]) { return arr.clone() }
    let (by_axis, arr_len, arrs_count)  = arr.by_axis_vars(0);
    let arrs = (0..arrs_count)
        .map(|i| by_axis.subarray(i * arr_len, (i + 1) * arr_len).sort(None))
        .collect::<Vec<Array<S>>>();
    let shape = vec![std::cmp::min(arrs_count, arr_len), std::cmp::min(arrs_count, arr_len)];
    let elems = if arr_len < arrs_count { (0 .. arr_len).flat_map(|i| arrs[i].get_elements_vec()).collect() }
    else { (0 .. arrs_count).flat_map(|i| arrs[i].subarray(0, arrs_count).get_elements_vec()).collect() };
    Array::new(elems, shape)
}
