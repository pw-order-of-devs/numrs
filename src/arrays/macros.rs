use crate::prelude::*;

pub fn array_1d<S: ArrayType>(values: Vec<S>) -> Array<S> {
    Array::new(values.clone(), vec![values.len()])
}

pub fn array_2d<S: ArrayType>(values: Vec<S>, shape: (usize, usize)) -> Array<S> {
    Array::new(values, vec![shape.0, shape.1])
}

pub fn array_3d<S: ArrayType>(values: Vec<S>, shape: (usize, usize, usize)) -> Array<S> {
    Array::new(values, vec![shape.0, shape.1, shape.2])
}

pub fn array_ma_1d<S: ArrayType>(values: Vec<S>, mask: Vec<usize>) -> Array<S> {
    Array::new_masked(values.clone(), vec![values.len()], mask)
}

pub fn array_ma_2d<S: ArrayType>(values: Vec<S>, shape: (usize, usize), mask: Vec<usize>) -> Array<S> {
    Array::new_masked(values, vec![shape.0, shape.1], mask)
}

pub fn array_ma_3d<S: ArrayType>(values: Vec<S>, shape: (usize, usize, usize), mask: Vec<usize>) -> Array<S> {
    Array::new_masked(values, vec![shape.0, shape.1, shape.2], mask)
}

pub fn flatten3d<S: ArrayType>(vec: Vec<Vec<Vec<S>>>) -> Vec<Vec<S>> {
    vec.into_iter().map(|i| i.into_iter().flatten().collect::<Vec<S>>().to_vec()).collect()
}

pub fn flatten2d<S: ArrayType>(vec: Vec<Vec<S>>) -> Vec<S> {
    vec.into_iter().flatten().collect::<Vec<S>>()
}

#[macro_export]
macro_rules! array {
    ($([$([$($x:expr),* $(,)*]),+ $(,)*]),+ $(,)*) => {{
        let vec3d = vec![$(vec![$(vec![$($x,)*],)*],)*];
        let flat_vec = flatten2d(flatten3d(vec3d.clone()));
        array_3d(flat_vec, (vec3d.len(), vec3d[0].len(), vec3d[0][0].len()))
    }};
    ($([$($x:expr),* $(,)*]),+ $(,)*) => {{
        let vec2d = vec![$(vec![$($x,)*],)*];
        let flat_vec = flatten2d(vec2d.clone());
        array_2d(flat_vec, (vec2d.len(), vec2d[0].len()))
    }};
    ($($x:expr),* $(,)*) => {{
        array_1d(vec![$($x,)*])
    }};
}

#[macro_export]
macro_rules! array_ma {
    ($([$([$($x:expr),* $(,)*]),+ $(,)*]),+ $(,)*) => {{
        let vec3d_el = vec![$(vec![$(vec![$($x.0,)*],)*],)*];
        let vec3d_ma = vec![$(vec![$(vec![$($x.1,)*],)*],)*];
        let flat_vec_el = flatten2d(flatten3d(vec3d_el.clone()));
        let flat_vec_ma = flatten2d(flatten3d(vec3d_ma.clone()));
        array_ma_3d(flat_vec_el, (vec3d_el.len(), vec3d_el[0].len(), vec3d_el[0][0].len()), flat_vec_ma)
    }};
    ($([$($x:expr),* $(,)*]),+ $(,)*) => {{
        let vec2d_el = vec![$(vec![$($x.0,)*],)*];
        let vec2d_ma = vec![$(vec![$($x.1,)*],)*];
        let flat_vec_el = flatten2d(vec2d_el.clone());
        let flat_vec_ma = flatten2d(vec2d_ma.clone());
        array_ma_2d(flat_vec_el, (vec2d_el.len(), vec2d_el[0].len()), flat_vec_ma)
    }};
    ($($x:expr),* $(,)*) => {{
        array_ma_1d(vec![$($x.0,)*], vec![$($x.1,)*])
    }};
}
