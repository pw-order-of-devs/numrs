use std::cmp::Ordering;

use crate::prelude::*;

pub trait Not {
    fn not(&self) -> bool;
}

impl Not for bool {
    fn not(&self) -> bool {
        match self {
            true => false,
            false => true
        }
    }
}

pub trait FloatCmp<S> {
    fn approx_eq(&self, other: S) -> bool;
    fn approx_eq_ord(&self, other: S) -> Ordering;
    fn unwrap_nan(&self, other: S) -> Ordering;
}

impl <S: ArrayType>FloatCmp<S> for S {
    fn approx_eq(&self, other: S) -> bool {
        self.partial_cmp(&other).unwrap_or_else(|| Self::unwrap_nan(self, other)) == Ordering::Equal
    }
    fn approx_eq_ord(&self, other: S) -> Ordering {
        self.partial_cmp(&other).unwrap_or_else(|| Self::unwrap_nan(self, other))
    }
    fn unwrap_nan(&self, other: S) -> Ordering {
        if self.is_nan() && other.is_nan() { Ordering::Equal }
        else if self.is_nan() { Ordering::Greater }
        else { Ordering::Less }
    }
}

impl <S: ArrayType>FloatCmp<ArrayElem<S>> for ArrayElem<S> {
    fn approx_eq(&self, other: ArrayElem<S>) -> bool {
        self.partial_cmp(&other).unwrap_or_else(|| Self::unwrap_nan(self, other)) == Ordering::Equal
    }
    fn approx_eq_ord(&self, other: ArrayElem<S>) -> Ordering {
        self.partial_cmp(&other).unwrap_or_else(|| Self::unwrap_nan(self, other))
    }
    fn unwrap_nan(&self, other: ArrayElem<S>) -> Ordering {
        if self.is_nan() && other.is_nan() { Ordering::Equal }
        else if self.is_nan() { Ordering::Greater }
        else { Ordering::Less }
    }
}

impl <S: ArrayType>FloatCmp<S> for ArrayElem<S> {
    fn approx_eq(&self, other: S) -> bool {
        self.partial_cmp(&other.to_elem()).unwrap_or_else(|| Self::unwrap_nan(self, other)) == Ordering::Equal
    }
    fn approx_eq_ord(&self, other: S) -> Ordering {
        self.partial_cmp(&other.to_elem()).unwrap_or_else(|| Self::unwrap_nan(self, other))
    }
    fn unwrap_nan(&self, other: S) -> Ordering {
        if self.is_nan() && other.is_nan() { Ordering::Equal }
        else if self.is_nan() { Ordering::Greater }
        else { Ordering::Less }
    }
}
