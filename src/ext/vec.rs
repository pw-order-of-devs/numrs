use crate::prelude::*;

pub trait Stats<S: ArrayType> {
    fn sum(&self) -> S;
    fn mult(&self) -> S;
    fn mean(&self) -> S;
    fn std(&self, ddof: usize) -> S;
}

impl <S: ArrayType>Stats<S> for Vec<S> {

    fn sum(&self) -> S {
        self.iter().fold(S::zero(), |a, b| a.add(*b))
    }

    fn mult(&self) -> S {
        self.iter().fold(S::one(), |a, b| a.mul(*b))
    }

    fn mean(&self) -> S {
        self.sum().div(S::from_usize(self.len()))
    }

    fn std(&self, ddof: usize) -> S {
        let (len, mean, ddof) = (S::from_usize(self.len()), self.mean(), S::from_usize(ddof));
        let partial1 = S::one().div(len.sub(ddof));
        let partial2 = self.iter()
            .map(|x| (x.sub( mean)).powf(S::from_usize(2)))
            .collect::<Vec<S>>()
            .sum();
        (partial1.mul(partial2)).sqrt()
    }
}

pub trait ToMaskNeg<S> {
    fn to_mask_neg(&self) -> Vec<usize>;
}

impl <S: ArrayType>ToMaskNeg<S> for Vec<S> {
    fn to_mask_neg(&self) -> Vec<usize> {
        self.iter().map(|i| usize::from(*i < S::zero())).collect()
    }
}

pub trait FilterByMask<S> {
    fn filter_by_mask(&self) -> Vec<S>;
}

impl <S: ArrayType>FilterByMask<S> for Vec<(S, usize)> {
    fn filter_by_mask(&self) -> Vec<S> {
        self.iter().filter(|i| i.1 == 0).map(|i| i.0).collect()
    }
}

pub trait NanOps<S> {
    fn nan_to_zero(&self) -> Vec<S>;
    fn one_if_nan_else_zero(&self) -> Vec<usize>;
}

impl <S: ArrayType>NanOps<S> for Vec<S> {

    fn nan_to_zero(&self) -> Vec<S> {
        self.iter().map(|i| if i.is_nan() { S::zero() } else { *i }).collect()
    }

    fn one_if_nan_else_zero(&self) -> Vec<usize> {
        self.iter().map(|i| usize::from(i.is_nan())).collect()
    }
}

pub trait MoveElement<S> {
    fn move_elem(&mut self, from: usize, to: usize);
}

impl <S: ArrayType>MoveElement<S> for Vec<S> {

    fn move_elem(&mut self, from: usize, to: usize) {
        let tmp = self.remove(from);
        self.insert(to, tmp);
    }
}

pub trait Concatenate<S: ArrayType, A: ArrayBase<S>> {
    fn concatenate(&self, axis: Option<usize>) -> A;
}

impl <S: ArrayType, A: ArrayBase<S> + ArrayAxis<S>>Concatenate<S, A> for Vec<A> {
    fn concatenate(&self, axis: Option<usize>) -> A {
        if self.len() < 2 { return self.first().unwrap_or(&A::empty()).clone() }
        let mut cloned = self.clone();
        let arr = cloned.remove(0);
        arr.concatenate(&cloned, axis)
    }
}

pub fn lexsort<S1: ArrayType, S2: ArrayType>(arr1: &Array<S1>, arr2: &Array<S2>) -> Array<usize> {
    assert_eq!(arr1.len(), arr2.len(), "Arrays should be equal in length");
    assert!(!arr1.is_empty() && !arr2.is_empty(), "Arrays should not be empty");
    let (mut arr1m, mut arr2m) = (arr1.ravel(), arr2.ravel());
    let mut res = vec![];
    let mut idx = arr1.len();
    while idx > 0 {
        let min = arr2m.min(None, None).single();
        let min_len = arr2m.filter(|i| *i == min).len();
        if min_len == 1 {
            let pos = arr2.get_elements().iter().position(|i| *i == min).unwrap();
            let rm_pos = arr2m.get_elements().iter().position(|i| *i == min).unwrap();
            res.push(pos);
            let (mut vec1, mut vec2) = (arr1m.get_elements(), arr2m.get_elements());
            vec1.remove(rm_pos); vec2.remove(rm_pos);
            idx -= 1;
            arr1m = Array::new_elems_flat(vec1); arr2m = Array::new_elems_flat(vec2);
        } else {
            let min_pos = _get_min_pos(arr2.clone(), arr1.clone(), min);
            let mut min_pos_rm = _get_min_pos(arr2m.clone(), arr1m.clone(), min);
            let (mut vec1, mut vec2) = (arr1m.get_elements(), arr2m.get_elements());
            (0 .. min_pos.len()).for_each(|i| {
                res.push(min_pos[i]);
                vec1.remove(min_pos_rm[i]); vec2.remove(min_pos_rm[i]);
                min_pos_rm = min_pos_rm.iter().map(|it| if *it > min_pos_rm[i] { it - 1 } else { *it }).collect();
            });
            idx -= min_pos.len();
            arr1m = Array::new_elems_flat(vec1); arr2m = Array::new_elems_flat(vec2);
        }
    };
    Array::new_flat(res)
}

fn _get_min_pos<S1: ArrayType, S2: ArrayType>(arr: Array<S1>, sorter: Array<S2>, min: ArrayElem<S1>) -> Vec<usize> {
    let res = arr.get_elements()
        .into_iter().enumerate()
        .filter(|(_, v)| *v == min).map(|i| i.0).collect::<Vec<usize>>();
    let mut sorter = sorter
        .filter_enumerate(|i, _| res.contains(&i))
        .get_elements_vec()
        .into_iter().enumerate()
        .map(|(i, v)| (res[i], v))
        .collect::<Vec<(usize, S2)>>();
    sorter.sort_by(|a, b| a.1.approx_eq_ord(b.1));
    sorter.iter().map(|i| i.0).collect()
}
